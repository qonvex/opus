<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{

    use SoftDeletes;

    protected $hidden=['user_id','status','created_at','updated_at'];

    public function delete(){
        $this->work_category_comments()->delete();
        $this->prices()->delete();

        return parent::delete();
    }

    public function project_progress(){

        return $this->belongsTo('App\ProjectProgress');

    }

    public function customer(){

        return $this->belongsTo('App\Customer');

	}
	
	public function bill(){

		return $this->hasOne('App\Bill');
		
	}

    public function prices(){
        
        return $this->hasMany('App\ProjectPriceAssignment');

    }

    public function work_category_comments(){

        return $this->hasMany('App\ProjectWorkCategoryComment');

	}
	
    public function work_categories(){
        return $this->belongsToMany(
            'App\WorkCategory',
            'project_work_category_comments',
            'project_id',
            'work_category_id'
        )
        ->withPivot([
            'comments'
        ]);
    }

}
