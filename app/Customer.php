<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
	
	use SoftDeletes;

	protected $hidden=['user_id','status','created_at','updated_at'];

	protected $appends=['full_name'];

	public function delete(){
		$this->projects()->delete();
		$this->progress_dates()->delete();
		
		return parent::delete();
	}

	public function setToProspect(){
		$this->status=1;
		$this->save();
	}

	public function setToClient(){
		$this->status=2;
		$this->became_client_at=today()->toDateString();
		$this->save();
	}

	public function setToInactive(){
		$this->status=0;
		$this->save();
	}

	public function getFullNameAttribute(){
		return $this->name. ' ' . $this->surname;
	}

	public function getContractTypeNameAttribute(){
		return $this->contract_type()->get('name');
	}

	public function getMarketingAvenueNameAttribute(){
		return $this->marketing_avenue()->get('name');
	}

	public function contract_type(){

		return $this->belongsTo('App\ContractType');

	}

	public function marketing_avenue(){

		return $this->belongsTo('App\MarketingAvenue');

	}

	public function progress_dates(){

		return $this->hasMany('App\CustomerProspectProgress');

	}

	public function projects(){

		return $this->hasMany('App\Project');

	}

	public function prospect_progresses(){

		return $this
			->belongsToMany(
				'App\ProspectProgress',
				'customer_prospect_progresses',
				'customer_id',
				'prospect_progress_id'
			)
			->withPivot([
				'progress_date',
				'status',
				'user_id'
			])
			->where('status',1);

	}

}
