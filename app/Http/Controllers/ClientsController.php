<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Customer, ProjectType, ContractType};

use App\Traits\HashController;

class ClientsController extends Controller
{
	use HashController;

	/*
  	 *
	 * Validate that first contact date is on or before today (only if set)
	 * 
	 */
	private static function validateFirstContact($data){
		if (isset($data) && $data>today()->toDateString()){
			throw \Illuminate\Validation\ValidationException::withMessages([
			'first_contact' => ['La première date de contact doit être au plus tard à la date d\'aujourd\'hui.']
			]);
		}
	}

	/*
	 *
	 * Store prospect data from request object
	 * 
	 */
	private static function storeClientData($customer,$request){
		$customer->name=$request->name;
		$customer->surname=$request->surname;
		$customer->address=$request->address;
		$customer->city=$request->city;
		$customer->postal_code=$request->postal_code;
		$customer->email=$request->email;
		$customer->mobile=$request->mobile;
		$customer->telephone=$request->telephone;
		$customer->first_contact=$request->first_contact;
		$customer->budget=$request->budget;
		$customer->project_type_id=$request->project_type_id;
		$customer->contract_type_id=$request->contract_type_id;
		$customer->comments=$request->comments;
		$customer->status=2;
	}

	//	ROUTER
	public function route($item=null){

		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
			'show'=>null
		];

		if($item=='create'){
			$mode='create';
		}elseif($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

	}

	//	INTERNAL VERSION
	public function common(){
		$project_types=ProjectType::all();
		$contract_types=ContractType::all();

		$data=(object)compact(
			'project_types',
			'contract_types'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
  	public function index(){

		$page=Customer::where('status',2)
		->with(
			'contract_type',
			'prospect_progresses'
		)
		->paginate(100);
		
		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
		$clients=$page->items();

		$data=(object)compact(
			'current_page',
			'last_page',
			'clients'
		);

		$this->hashAppend($data);

		return $data;
	}
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function show($id){
		$client_id=$id;
		$client=Customer::with('projects.prices')->findOrFail($id);

		$data=(object)compact(
			'client_id',
			'client'
		);
	
		$this->hashAppend($data);

		return $data;
	}
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API-ONLY
	public function _store(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;
		
		/*
		*
		* REGULAR VALIDATION
		*
		*/
		$request->validate([
			'data.name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
			'data.budget' => 'nullable|between:1,99999999.99',
			'data.first_contact' => 'nullable|date'
		]);
			
		//STRUCTURIZE DATA
		$request=(object)$request->data;
			
		/*
		 *
		 * CUSTOM VALIDATION
		 *
		 */

		// FIRST CONTACT VALIDATION (SOMETIMES)
		self::validateFirstContact($request->first_contact??null);

		/*
		 *
		 * STORING CLIENT DATA
		 *
		 */
		$customer=new Customer;
		$customer->user_id=$userId;

		self::storeClientData($customer,$request);
		$customer->save();
		$customer->created_at=null;
		$customer->save();
		
		//RETURN NEW CLIENT URL AND ID
		$data=(object)[
			'url'=>'/clients/'.$customer->id,
			'id'=>$customer->id
		];

		$data=json_encode($data);

		return $data;
		
	}

	//	API-ONLY
	public function _update(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 *
		 */
		$request->validate([
		'data.name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
		'data.budget' => 'nullable|between:1,99999999.99',
		'data.first_contact' => 'nullable|date'
		]);

		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 *
		 */

		// FIRST CONTACT VALIDATION (SOMETIMES)
		self::validateFirstContact($request->first_contact??null);

		/*
		 *
		 * FIND CLIENT
		 * 
		 */
		$customer=Customer::findOrFail($request->id);

		/*
		 *
		 * STORING CLIENT DATA
		 *
		 */
		self::storeClientData($customer,$request);

		$customer->save();

		//RETURN CLIENT URL AND ID
		$data=(object)[
			'url'=>'/clients/'.$customer->id,
			'id'=>$customer->id
		];

		$data=json_encode($data);

		return $data;
	}

	//	API_ONLY
	public function _destroy(Request $request){
		try{
			Customer::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
			Customer::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
	}

}
