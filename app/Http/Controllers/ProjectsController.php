<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{ProjectProgress, Project, Customer, WorkCategory, Work, ProjectPriceAssignment, ProjectWorkCategoryComment};

use App\Traits\HashController;

class ProjectsController extends Controller
{
	use HashController;

	/*
	 *
	 * Store project data from request object
	 * 
	 */
	private static function storeProjectData($project,$request){
		$project->name=$request->name;
		$project->customer_id=$request->client_id;
		$project->address=$request->address;
		$project->city=$request->city;
		$project->postal_code=$request->postal_code;
		$project->comments=$request->comments;
		$project->description=$request->description;
		$project->objectives=$request->objectives;
		$project->project_progress_id=$request->project_progress_id;
		$project->sell_date=isset($request->sell_date)?$request->sell_date:null;
		$project->start_date=isset($request->start_date)?$request->start_date:null;
		$project->end_date=isset($request->end_date)?$request->end_date:null;
		$project->total_ttc=$request->total_ttc;
	}

    //  ROUTER
    public function route($item=null){
        
		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
			'create'=>null,
			'show'=>null
		];

		if($item=='create'){
			$mode='create';
			$modes->create=$this->create();
		}elseif($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

    }

	//	INTERNAL VERSION
	public function common(){
		$project_progresses=ProjectProgress::all();

		$data=(object)compact(
			'project_progresses'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function index(){
		
		$page=Project::where('status',1)->with('project_progress','customer')->paginate(100);
		
		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
		$projects=$page->items();

		$data=(object)compact(
			'current_page',
			'last_page',
			'projects'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function create(){

		$work_categories=WorkCategory::where('status',1)->with('companies','works','works.prices')->get();
		
		$data=(object)compact(
			'work_categories'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _create(Request $request){
		
		$data=$this->create();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function show($id){
		$work_categories=WorkCategory::where('status',1)->with([
			'companies'=>function($query){
				$query->select('companies.id','companies.name');
			},
			'works',
			'works.prices'
		])
		->get();
		$project_id=$id;
		$project=Project::with([
			'customer',
			'prices',
			'work_category_comments',
			'bill'=>function($query){
				$query->select(
					'id',
					'project_id'
				);
			}
		])
		->findOrFail($id);

		$data=(object)compact(
			'work_categories',
			'project_id',
			'project'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API ONLY
	public function _store(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 * 
		 */
		$request->validate([
			
			//no validation yet

		]);

		//STRUCTURIZE DATA
		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 * 
		 */

		if (!$request->name){
			abort(422,'Un nom de projet est requis');
		}

		if (!$request->client_id || !Customer::find($request->client_id)){
			abort(422,'S\'il vous plaît sélectionner un client');
		}

		/*
		 *
		 * STORING PROJECT DATA
		 * 
		 */
		$project=new Project;
		$project->user_id=$userId;

		self::storeProjectData($project,$request);
		$project->save();

		//ATTACH PRICES ARRAY
		foreach($request->work_categories??[] as $work_category){
			$work_category=(object)$work_category;

			if (!property_exists($work_category,'quantity_by_work_id')) continue;

			if (!property_exists($work_category,'company_id')) continue;

			if (gettype($work_category->quantity_by_work_id)=='array'){
				if (!(array_sum($work_category->quantity_by_work_id)>0)) continue;
			}


			$comment=new ProjectWorkCategoryComment;

			$comment->project_id=$project->id;
			$comment->work_category_id=$work_category->work_category_id;
			$comment->user_id=$userId;
			$comment->company_id=$work_category->company_id;
			$comment->comments=$work_category->comments??'';
			
			$comment->save();

			foreach($work_category->quantity_by_work_id as $work_id=>$quantity){
				if (!$quantity) continue;
				$work=Work::findOrFail($work_id);
				$company_price=$work->getCompanyPrice($work_category->company_id);
				if ($company_price===null) continue;
				$project_price_assignment=new ProjectPriceAssignment;
				$project_price_assignment->user_id=$userId;

				//REFERENCES
				$project_price_assignment->project_id=$project->id;
				$project_price_assignment->company_id=$work_category->company_id;
				$project_price_assignment->work_comment_id=$comment->id;
				$project_price_assignment->work_id=$work_id;
				
				//DATA
				$project_price_assignment->price_per_unit=$company_price;
				$project_price_assignment->quantity=$quantity;
				$project_price_assignment->tax=$work_category->tax_by_work_id[$work_id];
				$project_price_assignment->margin=$work_category->margin_by_work_id[$work_id];

				//SAVE
				$project_price_assignment->save();
					
			}
		}

		//RETURN NEW PROJECT URL AND ID
		$data=(object)[
			'url'=>'/projects/'.$project->id,
			'id'=>$project->id
		];

		$data=json_encode($data);

		return $data;

	}

	//	API ONLY
	public function _update(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 * 
		 */
		$request->validate([
			
			//no validation yet

		]);

		//STRUCTURIZE DATA
		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 * 
		 */

		 //no validation yet

		/*
		 *
		 * FIND PROJECT
		 * 
		 */
		$project=Project::findOrFail($request->id);

		/*
		 *
		 * STORE PROJECT DATA
		 * 
		 */
		self::storeProjectData($project,$request);

		$project->save();

		
		//REDO PRICES ARRAY
		$project->work_category_comments()->delete();

		foreach($request->work_categories??[] as $work_category){
			$work_category=(object)$work_category;

			if (!property_exists($work_category,'quantity_by_work_id')) continue;

			if (!property_exists($work_category,'company_id')) continue;

			if (gettype($work_category->quantity_by_work_id)=='array'){
				if (!(array_sum($work_category->quantity_by_work_id)>0)) continue;
			}


			$comment=new ProjectWorkCategoryComment;

			$comment->project_id=$project->id;
			$comment->work_category_id=$work_category->work_category_id;
			$comment->user_id=$userId;
			$comment->company_id=$work_category->company_id;
			$comment->comments=$work_category->comments??'';
			
			$comment->save();

			foreach($work_category->quantity_by_work_id as $work_id=>$quantity){
				if (!$quantity) continue;
				$work=Work::findOrFail($work_id);
				$company_price=$work->getCompanyPrice($work_category->company_id);
				if ($company_price===null) continue;
				$project_price_assignment=new ProjectPriceAssignment;
				$project_price_assignment->user_id=$userId;

				//REFERENCES
				$project_price_assignment->project_id=$project->id;
				$project_price_assignment->company_id=$work_category->company_id;
				$project_price_assignment->work_comment_id=$comment->id;
				$project_price_assignment->work_id=$work_id;
				
				//DATA
				$project_price_assignment->price_per_unit=$company_price;
				$project_price_assignment->quantity=$quantity;
				$project_price_assignment->tax=$work_category->tax_by_work_id[$work_id];
				$project_price_assignment->margin=$work_category->margin_by_work_id[$work_id];

				//SAVE
				$project_price_assignment->save();
				
			}
		}
		
		//RETURN PROJECT URL AND ID
		$data=(object)[
			'url'=>'/projects/'.$project->id,
			'id'=>$project->id
		];

		$data=json_encode($data);

		return $data;

	}

    public function _destroy(Request $request){
		try{
			Project::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
			Project::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
    }
}
