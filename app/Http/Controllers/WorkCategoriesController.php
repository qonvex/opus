<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Work, WorkCategory, Company, CompanyWorkPrice};

use App\Traits\HashController;

class WorkCategoriesController extends Controller
{
    use HashController;

    //  ROUTER
    public function route($item=null, $item2=null){

        $mode;
		$common=$this->common();
		$modes=(object)[
            'index'=>null,
            'create'=>null,
			'show'=>null
		];

        if($item=='create'){
            $mode='create';
            $modes->create=$this->create();
		}elseif($item=='works'){
			$mode='show';
			$modes->show=app('App\Http\Controllers\WorksController')->show($item2);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

        return $data;

    }

    //  INTERNAL VERSION
    public function common(){
        $placeholder=[];

        $data=(object)compact(
            'placeholder'
        );

        $this->hashAppend($data);

        return $data;

    }
    //	API VERSION
    public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API-ONLY
	public function _show(Request $request){
		return app('App\Http\Controllers\WorksController')->_show($request);
	}
    
    //  INTERNAL VERSION
    public function create(){
        
        $work_categories=WorkCategory::where('status',1)->with('companies','works','works.prices')->get();

        $data=(object)compact(
            'work_categories'
        );

        $this->hashAppend($data);

        return $data;

    }
    //	API VERSION
	public function _create(Request $request){

		$data=$this->create();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
    }
    
    //  INTERNAL VERSION
    public function index(){
        
        $work_categories=WorkCategory::where('status',1)->with('companies','works','works.prices')->get();

        $data=(object)compact(
            'work_categories'
        );

        $this->hashAppend($data);

        return $data;

    }
    //	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
    }

    //  API ONLY
    public function _storeWorks(Request $request){

        //SET THE USER ID
        $userId=auth()->user()->id;
        
        $work=new Work;

        $work->work_category_id=$request->category_id;
        $work->name=$request->name;
        $work->unit=$request->unit;

        $work->save();

        $companyWorkPrices=$request->company_price??[];

        foreach ($companyWorkPrices as $id=>$price){
            if (isset($price))
            $work->companies()->attach($id, ['user_id'=>$userId,'price_per_unit'=>$price]);
        }

		$response=json_encode(["type"=>"alert-success","message"=>"success"]);

        return $response;
	}
		
    public function editWork($id){

        $work=Work::findOrFail($id);
        $workCategory=WorkCategory::find($work->work_category_id);
        $companies=[];

        foreach($workCategory->companies->makeHidden('pivot') as $company){
            $companies[$company->id]=$company->name;
        }

        $data=$work->only('id','work_category_id','name','unit');
        $data['companies']=$companies;
        $data['prices']=$work->getPrices();
        $data['work_category']=$workCategory->name;

        return view('pages.work_categories.works.edit')->with('work',$data);			
    }
    
    public function updateWork($id, Request $request){

        //SET THE USER ID
        $userId=auth()->user()->id;

        $work=Work::findOrFail($id);

        $workCategory=$work->workCategory;
        $companies=$workCategory->companies;

        $work->name=$request->name;
        $work->unit=$request->unit;

        $work->save();


        
        $response=json_encode(["type"=>"alert-success","message"=>$id]);
        
        return $response;
    }



}
