<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\{Bill, Objective, Project, ProjectProgress};

use App\Traits\HashController;

class DashboardController extends Controller
{
	use HashController;
    
    //  ROUTER
    public function route(){
        
		$mode='index';
		$modes=(object)[
			'index'=>$this->index(),
        ];

		$data=(object)compact(
			'mode',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

    }

    //  INTERNAL VERSION
    public function index(){

		$project_progresses=ProjectProgress::all();

		$bills=Project::whereNotNull('sell_date')->orderBy('sell_date','ASC')->select('total_ttc','sell_date')->get();

		$objectives=Objective::all();
		
		$prospect_counts=DB::select('
			select
				year(first_contact) as year,
				month(first_contact) as month,
				count(first_contact) as count
			from customers
			WHERE first_contact IS NOT NULL
			GROUP BY year(first_contact), month(first_contact)
		');
		$states=[
			'RAF'=>DB::select('
				select
					year(progress_date) as year,
					month(progress_date) as month,
					count(progress_date) as count
				from customer_prospect_progresses
				WHERE prospect_progress_id = 1
				GROUP BY year(progress_date), month(progress_date)
			'),
			'VALIDE'=>DB::select('
				select
					year(progress_date) as year,
					month(progress_date) as month,
					count(progress_date) as count
				from customer_prospect_progresses
				WHERE prospect_progress_id = 6
				GROUP BY year(progress_date), month(progress_date)
			'),
			'VENTE'=>DB::select('
				select
					year(became_client_at) as year,
					month(became_client_at) as month,
					count(became_client_at) as count
				from customers
				WHERE became_client_at IS NOT NULL
				GROUP BY year(became_client_at), month(became_client_at)
			')
		];
		$projects=Project::where('project_progress_id','!=','5')->with([
			'customer'=>function($query){
				$query->select('id','name','surname','city');
			}
		])->get();

        $data=(object)compact(
			'bills',
			'objectives',
			'prospect_counts',
			'states',
			'projects'
		);

        $this->hashAppend($data);

        return $data;
    }
}
