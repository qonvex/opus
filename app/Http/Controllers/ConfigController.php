<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Objective};

use App\Traits\HashController;

class ConfigController extends Controller
{
	use HashController;

	/*
	 *
	 * Store project data from request object
	 * 
	 */
	private static function storeObjectiveData($objective,$request){
        $objective->year=$request->year;
		$objective->jan=$request->jan;
		$objective->feb=$request->feb;
		$objective->mar=$request->mar;
		$objective->apr=$request->apr;
		$objective->may=$request->may;
		$objective->jun=$request->jun;
		$objective->jul=$request->jul;
		$objective->aug=$request->aug;
		$objective->sep=$request->sep;
		$objective->oct=$request->oct;
		$objective->nov=$request->nov;
		$objective->dec=$request->dec;
	}

    //  ROUTER
    public function route($item=null){
        
		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
			'create'=>null,
			'show'=>null
		];

		if($item=='create'){
			$mode='create';
			$modes->create=$this->create();
		}elseif($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

    }

	//	INTERNAL VERSION
	public function common(){
		$placeholder=[];

		$data=(object)compact(
            'placeholder'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function index(){
		
		$page=Objective::paginate(100);
		
		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
		$objectives=$page->items();

		$data=(object)compact(
			'current_page',
			'last_page',
			'objectives'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function create(){

        $placeholder=[];
		
		$data=(object)compact(
			'placeholder'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _create(Request $request){
		
		$data=$this->create();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function show($id){
		$objective_id=$id;
		$objective=Objective::findOrFail($id);

		$data=(object)compact(
			'objective_id',
			'objective'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API ONLY
	public function _store(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 * 
		 */
		$request->validate([
			
			//no validation yet

		]);

		//STRUCTURIZE DATA
		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 * 
		 */

		 if (Objective::where('year','=',$request->year)->count()){
             abort(422,'L\'année existe déjà');
         }

		/*
		 *
		 * STORING PROJECT DATA
		 * 
		 */
		$objective=new Objective;
		$objective->user_id=$userId;

		self::storeObjectiveData($objective,$request);
		$objective->save();

		//RETURN NEW OBJECTIVE URL AND ID
		$data=(object)[
			'url'=>'/config/'.$objective->id,
			'id'=>$objective->id
		];

		$data=json_encode($data);

		return $data;

	}

	//	API ONLY
	public function _update(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 * 
		 */
		$request->validate([
			
			//no validation yet

		]);

		//STRUCTURIZE DATA
		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 * 
		 */

		 
        if (Objective::where('year','=',$request->year)->where('id','!=',$request->id)->count()){
            abort(422,'L\'année existe déjà');
        }

		/*
		 *
		 * FIND PROJECT
		 * 
		 */
		$objective=Objective::findOrFail($request->id);

		/*
		 *
		 * STORE PROJECT DATA
		 * 
		 */
		self::storeObjectiveData($objective,$request);

		$objective->save();

		//RETURN OBJECTIVE URL AND ID
		$data=(object)[
			'url'=>'/config/'.$objective->id,
			'id'=>$objective->id
		];

		$data=json_encode($data);

		return $data;

	}

    public function _destroy(Request $request){
		try{
			Objective::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
			Objective::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
    }
}
