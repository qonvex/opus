<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Work};

use App\Traits\HashController;

class WorksController extends Controller
{
    use HashController;
    
    //  INTERNAL VERSION
    public function show($id){
		$work_id=$id;
        $work=Work::with([
            'work_category'=>function($query){
				$query->with(['companies'=>function($query){
					$query->select('companies.id','companies.name');
				}]);
			},
            'prices',
        ])
        ->findOrFail($id);

		$data=(object)compact(
			'work_id',
			'work'
		);
	
		$this->hashAppend($data);

		return $data;
    }

    //  API VERSION
    public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

    }

    //  API VERSION
    public function _store(Request $request){
        //SET THE USER ID
        $userId=auth()->user()->id;

        /*
         *
         * REGULAR VALIDATION
         *
         */
        $request->validate([
            'data.name'=>'required',
            'data.unit'=>'required',
        ]);

        $request=(object)$request->data;

        /*
         *
         * CUSTOM VALIDATION
         *
         */
        
        $work=new Work;

        $work->user_id=$userId;
        $work->work_category_id=$request->category_id;
        $work->name=$request->name;
        $work->unit=$request->unit;

        $companyWorkPrices=$request->prices??[];
		
		$work->save();

        foreach ($companyWorkPrices as $id=>$price){
            if ($price===null) continue;
            $work->companies()->attach($id, ['user_id'=>$userId,'price_per_unit'=>$price]);
        }
		
		$response=json_encode(["type"=>"alert-success","message"=>"success"]);

        return $response;
    }

    //  API VERSION
    public function _update(Request $request){
        //SET THE USER ID
        $userId=auth()->user()->id;

        /*
         *
         * REGULAR VALIDATION
         *
         */
        $request->validate([
            //NO VALIDATION YET
        ]);

        $request=(object)$request->data;

        /*
         *
         * CUSTOM VALIDATION
         *
         */

        //PUT CUSTOM VALIDATION HERE
        
        $work=Work::findOrFail($request->id);

        $work->name=$request->name;
        $work->unit=$request->unit;

        $work->save();

        $companyWorkPrices=$request->prices??[];

        $work->prices()->delete();

        foreach ($companyWorkPrices as $id=>$price){
            if ($price===null) continue;
            $work->companies()->attach($id, ['user_id'=>$userId,'price_per_unit'=>$price]);
        }

		$response=json_encode(["type"=>"alert-success","message"=>"success"]);

        return $response;
    }

    //  API VERSION
    public function _destroy(Request $request){
		try{
			Work::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
            Work::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
    }

}
