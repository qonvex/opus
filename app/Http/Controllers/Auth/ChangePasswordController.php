<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{

    public function showChangePasswordForm(){
        return view('auth.change');
    }

    public function changePassword(){
        
        $user=auth()->user();

        $current=request()->current_password;
        $new=request()->new_password;
        $confirm=request()->confirm_password;

        if ($new==$confirm && Hash::check($current,$user->password)){
            $user->password=bcrypt($new);
            $user->save();
            return redirect('/password/success');
        }

        return view('auth.change')->with(['error'=>'Vos coordonnées ne correspondent pas. Veuillez réessayer.']);
    }

    public function success(){
        return view('auth.success');
    }

}
