<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Customer, Company, Project};

use App\Traits\HashController;

class PagesController extends Controller
{

	use HashController;

	//	DEFAULT HOMEPAGE
	private static $homepage='dashboard';

	//	INTERNAL VERSION
	public function links(){

		$links=(object)[];

		$links->dashboard='/dashboard';
		$links->prospects='/prospects';
		$links->clients='/clients';
		$links->companies='/companies';
		$links->work_categories='/work_categories';
		$links->projects='/projects';
		$links->bills='/bills';
		$links->config='/config';

		return $links;

	}
	//API VERSION
	public function _links(Request $request){
		
		$data=$this->links();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	public function app($resource=null,$item=null,$item2=null,Request $request){

		/*		
		 * ASSEMBLE GENERAL DATA
		 */
		$links=self::links();
		$data=(object)[
			'links'=>$links,
			'resource'=>$resource??(self::$homepage),
			'item'=>$item
		];

		/*
		 * GET PAGE-SPECIFIC DATA
		 */
		if(method_exists($this,$data->resource)){
			$page=$this->{$data->resource}($item,$item2);
		}else{
			$page=$this->{self::$homepage}();
		}

		/*
		 * ATTACH ADDITIONAL DATA
		 */
		$data->page=$page;

		/*
		 * SEND THE APP
		 */
		return view('app')->with('data',$data)->with('version',$this->version())->with('gzsize',$this->gzsize());

	}

	public function dashboard(){
		
		return app('App\Http\Controllers\DashboardController')->route();

	}

	//	API VERSION
	public function _dashboard(Request $request){

		$data=$this->dashboard();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function prospects($item=null){

		return app('App\Http\Controllers\ProspectsController')->route($item);

	}
	//	API VERSION
	public function _prospects(Request $request){

		$data=$this->prospects();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function clients($item=null){

		return app('App\Http\Controllers\ClientsController')->route($item);

	}
	//	API VERSION
	public function _clients(Request $request){

		$data=$this->clients();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function companies($item=null){

		return app('App\Http\Controllers\CompaniesController')->route($item);

	}
	//	API VERSION
	public function _companies(Request $request){

		$data=$this->companies();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function projects($item=null){

		return app('App\Http\Controllers\ProjectsController')->route($item);

	}
	//	API VERSION
	public function _projects(Request $request){

		$data=$this->projects();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function work_categories($item=null, $item2=null){

		return app('App\Http\Controllers\WorkCategoriesController')->route($item, $item2);

	}
	//	API VERSION
	public function _work_categories(Request $request){

		$data=$this->work_categories();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function bills($item=null){

		return app('App\Http\Controllers\BillsController')->route($item);

	}
	//	API VERSION
	public function _bills(Request $request){

		$data=$this->bills();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function config($item=null){

		return app('App\Http\Controllers\ConfigController')->route($item);

	}
	//	API VERSION
	public function _config(Request $request){

		$data=$this->config();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API-ONLY
	public function _search(Request $request){

		$queries=$request->q;

		if (!$queries || count($queries)<1) return;

		$first=strtolower($queries[0]);

		// search customers
		$customers=Customer::select(
			'id','name','surname','city', 'mobile','telephone','email','status'
		)
		->where('name','like',"%$first%")
		->orWhere('surname','like',"%$first%");

		$customers=array_reduce(array_splice($queries,1),function($previous,$term){
			return $previous
			->where('name','like',"%$term%")
			->orWhere('surname','like',"%$term%");
		},$customers);

		$customers=$customers->limit(5)->get()->each(function($customer){
			$customer->makeVisible('status');
		});;

		//	search companies
		$companies=Company::select(
			'id','name','email','address','city', 'mobile','telephone'
		)
		->where('name','like',"%$first%")
		->orWhere('contact_person','like',"%$first%");

		$companies=array_reduce(array_splice($queries,1),function($previous,$term){
			return $previous
			->where('name','like',"%$term%")
			->orWhere('contact_person','like',"%$term%");
		},$companies);

		$companies=$companies->limit(5)->get()->each(function($customer){
			$customer->makeVisible('status');
		});;

		//	search projects
		$projects=Project::select(
			'id','name','address','city', 'customer_id'
		)
		->with(['customer'=>function($query){
			$query->select('customers.name','customers.surname');
		}])
		->where('name','like',"%$first%");

		$projects=array_reduce(array_splice($queries,1),function($previous,$term){
			return $previous
			->where('name','like',"%$term%");
		},$projects);

		$projects=$projects->limit(5)->get()->each(function($customer){
			$customer->makeVisible('status');
		});;

		$data=(object)compact(
			'customers',
			'companies',
			'projects'
		);

		return json_encode($data);

	}

	public function version(){
		$mtime=filemtime(base_path('public/js/app.js'));
		$version=base_convert($mtime,10,36);

		return $version;
	}

	public function gzsize(){
		$file=base_path('public/js/app.js');
		$gzsize=strlen((file_get_contents($file)));

		return $gzsize;
	}

}
