<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Company, WorkCategory};

use App\Traits\HashController;

class CompaniesController extends Controller
{
    use HashController;

    private static function storeCompanyData($company,$request){

        $company->name=$request->name;
        $company->legal_form=$request->legal_form;
        $company->activity=$request->activity;
        $company->mobile=$request->mobile;
        $company->telephone=$request->telephone;
        $company->email=$request->email;
        $company->address=$request->address;
        $company->city=$request->city;
        $company->postal_code=$request->postal_code;
        $company->contact_person=$request->contact_person;
        $company->annual_date=$request->annual_date;
        $company->rcs_code=$request->rcs_code;
        $company->iban_code=$request->iban_code;
        $company->bic_code=$request->bic_code;
        $company->siren_code=$request->siren_code;
        $company->ape_code=$request->ape_code;
        $company->comments=$request->comments;
        $company->status=1;

    }

    private static function storeWorkCategories($company,$workCategories,$userId){
        $errors=[];
        foreach ($workCategories as $workCategoryId){
			if (!WorkCategory::find($workCategoryId)) continue;
            if (WorkCategory::isAvailable($workCategoryId)){
                $company->work_categories()->attach($workCategoryId,['user_id'=>$userId]);
            }
            else {
                $categoryName=WorkCategory::find($workCategoryId)->name;
                $errors[]='Unable to add work category assignment due to unavailability: '.$categoryName;
            }
        }
        return $errors;
    }

    //  ROUTER
    public function route($item=null){

		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
            'show'=>null,
            'create'=>null
		];

		if($item=='create'){
            $mode='create';
            $modes->create=$this->create();
		}elseif($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

        return $data;
        
    }

    //  INTERNAL VERSION
    public function common(){
        $work_categories=WorkCategory::all();

        $data=(object)compact(
            'work_categories'
        );

        $this->hashAppend($data);

        return $data;

    }
    //	API VERSION
    public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

    //  INTERNAL VERSION
    public function index(){

        $page=Company::where('status',1)->with('work_categories')->paginate(100);

		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
        $companies=$page->items();
        
		$data=(object)compact(
			'current_page',
			'last_page',
			'companies'
        );
        
        $this->hashAppend($data);
        
        return $data;
    }
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
    }
    
    //  INTERNAL VERSION
    public function create(){
        $work_categories_available=WorkCategory::getAvailable();
        
        $data=(object)compact(
            'work_categories_available'
        );

        $this->hashAppend($data);

        return $data;
    }
	//	API VERSION
	public function _create(Request $request){

		$data=$this->create();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
    }

    //  INTERNAL VERSION
    public function show($id){
        $work_categories_available=WorkCategory::getAvailable();
        $company_id=$id;
        $company=Company::with('work_categories')->findOrFail($id);

        $data=(object)compact(
            'work_categories_available',
            'company_id',
            'company'
        );
	
		$this->hashAppend($data);

		return $data;
    } 
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

    //  API ONLY
    public function _store(Request $request)
    {

        //SET THE USER ID
        $userId=auth()->user()->id;

        /*
         *
         * REGULAR VALIDATION
         *
         */
        $request->validate([
            'data.name'=>'required'
        ]);

        $request=(object)$request->data;

        /*
         *
         * CUSTOM VALIDATION
         *
         */

        //PUT CUSTOM VALIDATION SHIT HERE

        $company=new Company;
        
        //STORE COMPANY DATA
        self::storeCompanyData($company,$request);
        $company->save();
      
        //FIX CATEGORIES ARRAY
        $workCategoriesSelected=array_unique($request->work_category_ids??[]);

        //ATTACH CATEGORIES WHILE CATCHING ANY ERRORS
        $categoryErrors=self::storeWorkCategories($company,$workCategoriesSelected,$userId);
      
        //RETURN NEW COMPANY URL AND ID
		$data=(object)[
			'url'=>'/companies/'.$company->id,
			'id'=>$company->id
		];

		$data=json_encode($data);

		return $data;
    }

    //  API ONLY
    public function _update(Request $request)
    {

		$id=$request->data['id'];
        //SET THE USER ID
        $userId=auth()->user()->id;

        /*
         *
         * REGULAR VALIDATION
         *
         */
        $request->validate([
            //NO VALIDATION YET
        ]);

        $request=(object)$request->data;

        /*
        *
        * CUSTOM VALIDATION
        *
        */

        //PUT CUSTOM VALIDATION SHIT HERE

        $company=Company::findOrFail($id);

        //STORE COMPANY DATA
        self::storeCompanyData($company,$request);
        $company->save();
      
        //FIX CATEGORIES ARRAY
        $workCategoryIds=WorkCategory::all()->pluck('id');
        $currentWorkCategories=$company->work_categories;
        $requestWorkCategoryIds=array_unique($request->work_category_ids??[]);
        $currentWorkCategoryIds=$currentWorkCategories->pluck('id')->toArray();
        
        $categoriesToAttach=[];

        //loop through all available categories in the database
        foreach ($workCategoryIds as $workCategoryId){
            if ($workCategoryId==0) continue;

            //IF IN CURRENT IDs AND...
            if (in_array($workCategoryId, $currentWorkCategoryIds)){
            
            //GET CATEGORY LINK
            $categoryPivot=$currentWorkCategories->find($workCategoryId)->pivot;
    
            //...IN REQUEST IDs THEN...
            if (in_array($workCategoryId, $requestWorkCategoryIds)){
    
               //DO NOTHING
    
            }else{      
            //..NOT IN REQUEST IDs THEN SET STATUS 0 (DEACTIVATE)
    
                //DEACTIVATE OLD PIVOT
                $company->work_categories()->detach($workCategoryId);
    
            }
            }else{
    
            
            //IF NOT IN CURRENT IDs and IN REQUEST IDs THEN SET ATTACH (ACTIVATE)
            if (in_array($workCategoryId, $requestWorkCategoryIds)){

                $categoriesToAttach[]=$workCategoryId;
    
            }else{
                //IF NOT IN CURRENT IDs and NOT IN REQUEST IDs THEN DO NOTHING (inactive - still inactive)
                
                //do nothing
    
            }
    
    
            }
  
        }

        //ATTACH CATEGORIES WHILE CATCHING ANY ERRORS
        $categoryErrors=self::storeWorkCategories($company,$categoriesToAttach,$userId);

        //RETURN COMPANY URL AND ID
        $data=(object)[
            'url'=>'/companies/'.$company->id,
            'id'=>$company->id
        ];

        $data=json_encode($data);

        return $data;
    }

    public function _destroy(Request $request){
		try{
			Company::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
            Company::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
    }
}
