<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{Bill, BillComment, BillPrice, Project};

use App\Traits\HashController;

class BillsController extends Controller
{
	use HashController;

	/*
	 *
	 * Store bill data from project object
	 * 
	 */
	private static function storeBillData($bill,$project){
		$bill->name=$project->name;
		$bill->customer_id=$project->customer_id;
		$bill->project_id=$project->id;
		$bill->address=$project->address;
		$bill->city=$project->city;
		$bill->postal_code=$project->postal_code;
		$bill->sell_date=$project->sell_date;
		$bill->start_date=$project->start_date;
		$bill->end_date=$project->end_date;
		$bill->total_ttc=$project->total_ttc;
	}

    //  ROUTER
    public function route($item=null){
        
		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
			'show'=>null
		];

		if($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

    }

	//	INTERNAL VERSION
	public function common(){
		$placeholder=[];

		$data=(object)compact(
			'placeholder'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
	public function index(){
		
		$page=Bill::with([
			'customer'=>function($query){
				$query->select(
					'customers.id',
					'customers.name',
					'customers.surname',
					'customers.email',
					'customers.mobile'
				)->withTrashed();
			}
		])
		->paginate(100);
		
		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
		$bills=$page->items();

		$data=(object)compact(
			'current_page',
			'last_page',
			'bills'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function show($id){
		$bill_id=$id;
		$bill=Bill::with([
			'customer'=>function($query){
				$query->select(
					'customers.id',
					'customers.name',
					'customers.surname',
					'customers.email',
					'customers.mobile',
					'customers.telephone'
				)->withTrashed();
			},
			'comments',
			'comments.company'=>function($query){
				$query->select(
					'companies.id',
					'companies.name'
				)->withTrashed();
			},
			'comments.work_category'=>function($query){
				$query->select(
					'work_categories.id',
					'work_categories.name'
				);
			},
			'comments.prices'=>function($query){
				$query->select(
					'bill_prices.bill_comment_id',
					'bill_prices.work_id',
					'bill_prices.price_per_unit',
					'bill_prices.quantity',
					'bill_prices.tax',
					'bill_prices.margin'
				);
			},
			'comments.prices.work'=>function($query){
				$query->select(
					'works.id',
					'works.name',
					'works.unit'
				)->withTrashed();
			}
		])
		->findOrFail($id);

		$data=(object)compact(
			'bill_id',
			'bill'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API ONLY
	public function _store(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		//STRUCTURIZE DATA
		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 * 
		 */

		 //no validation yet

		/*
		 *
		 * RETREIVING PROJECT DATA
		 * 
		 */
		$project=Project::with([
			'work_category_comments'=>function($query){
				$query->select(
					'project_work_category_comments.id',
					'project_work_category_comments.project_id',
					'project_work_category_comments.company_id',
					'project_work_category_comments.work_category_id',
					'project_work_category_comments.comments'
				);
			},
			'work_category_comments.prices'=>function($query){
				$query->select(
					'project_price_assignments.id',
					'project_price_assignments.work_comment_id',
					'project_price_assignments.work_id',
					'project_price_assignments.price_per_unit',
					'project_price_assignments.quantity',
					'project_price_assignments.tax',
					'project_price_assignments.margin'
				);
			}
		])->select(
			'id',
			'customer_id',
			'city',
			'name',
			'address',
			'postal_code',
			'sell_date',
			'start_date',
			'end_date'
		)->findOrFail($request->project_id);

		$bill=new Bill;
		$bill->user_id=$userId;

		$project=(object)$project;

		//CALCULATE TTC

		$totalTTC='0';

		bcscale(4);

		foreach ($project->work_category_comments as $comment){
			foreach ($comment->prices as $price){

				$currentTotal=$price->price_per_unit;

				$currentTotal=bcmul($currentTotal,$price->quantity);

				$currentTotal=bcmul($currentTotal,bcadd('1',bcdiv($price->tax,'100')));

				$currentTotal=bcmul($currentTotal,bcadd('1',bcdiv($price->margin,'100')));

				$totalTTC=bcadd($totalTTC,$currentTotal);

			}
		}

		$project->total_ttc=$totalTTC;


		//	STORE BILL DATA
		self::storeBillData($bill,$project);

		$bill->save();

		try{

			//	DUPLICATE COMMENTS AND PRICES

			foreach ($project->work_category_comments as $comment){
				
				$billComment=new BillComment;

				$billComment->user_id=$userId;
				$billComment->bill_id=$bill->id;
				$billComment->company_id=$comment->company_id;
				$billComment->work_category_id=$comment->work_category_id;
				$billComment->comments=$comment->comments;

				$billComment->save();

				foreach ($comment->prices as $price){

					$billPrice=new BillPrice;

					$billPrice->user_id=$userId;
					$billPrice->bill_id=$bill->id;
					$billPrice->bill_comment_id=$billComment->id;
					$billPrice->work_id=$price->work_id;
					$billPrice->price_per_unit=$price->price_per_unit;
					$billPrice->quantity=$price->quantity;
					$billPrice->tax=$price->tax;
					$billPrice->margin=$price->margin;

					$billPrice->save();

				}


			}

			$project=$bill->project;

			$project->project_progress_id=5;
			$project->save();

			$project->customer->setToClient();

			//RETURN NEW PROJECT URL AND ID
			$data=(object)[
				'url'=>'/bills/'.$bill->id,
				'id'=>$bill->id
			];

			$data=json_encode($data);

			return $data;

		}catch(Exception $e){

			//	if there are any errors, delete the generated bill

			$bill->delete();

			throw $e;

		}

	}

    public function _destroy(Request $request){
        //
    }
}
