<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


//  CONTROLLER IS API-ONLY

class FilesController extends Controller
{

    private static function recursive_mkdir($dest, $permissions=0755, $create=true){
        if(!is_dir(dirname($dest))){ self::recursive_mkdir(dirname($dest), $permissions, $create); }  
        elseif(!is_dir($dest)){ mkdir($dest, $permissions, $create); }
        else{return true;}
    }


    // HANDLES FILE UPLOAD
    public function _upload(Request $request){

        $fileContainers=[];

        
        foreach($request as $key=>$value){

            if (strpos($key,'file')===0){
                $fileContainers[]=$value;
                break;
            }

        }

        foreach($fileContainers[0]->all() as $path=>$container){

            $subfolder=[];

            preg_match('/(?<=_).*(?=_)/', $path, $subfolder);

            //if there was a subfolder specified, set it up
            $subfolder=count($subfolder)>0?'/'.$subfolder[0]:'';

            self::recursive_mkdir(base_path('/storage/app/uploads/'.
            $request->target_entity_type.
            '/'.
            $request->target_entity_id.
            $subfolder.'/'));

            foreach($container as $file){

                $file->move(
                    base_path('/storage/app/uploads/'.
                    $request->target_entity_type.
                    '/'.
                    $request->target_entity_id.
                    '/'.
                    $subfolder.
                    ''),
                
                    time().
                    '_'.
                    $file->getClientOriginalName()
                );

            }
        }

	}
	
	//	FETCH RESOURCE FILE (CAN BE SRC'D)
	public function _resource(Request $request){
		$path=base_path('storage/app/resources/'.$request->path);
		if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');

		return response()->file($path);
	}

	//	DELETE UPLOADED FILE
	public function _delete(Request $request){
        $path=base_path('storage/app/uploads/'.$request->path);
        if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');
		
		$success=unlink($path);

		$data=(object)compact(
			'success'
		);

        return json_encode($data);
	}

	//	FETCH FILE AS FILE (CAN BE SRC'D)
    public function _fetch(Request $request){
        $path=base_path('storage/app/uploads/'.$request->path);
        if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');

        return response()->file($path);

    }

    //  FETCH FILE IN BASE64
    public function _fetch64(Request $request){
        $path=base_path('storage/app/uploads/'.$request->path);
        if (!file_exists($path) || is_dir($path)) abort(404,'Fichier non trouvé');
        
        $content=file_get_contents($path);
        $file_data=base64_encode($content);

        $data=(object)compact(
            'file_data'
        );

        return json_encode($data);
    }

    //  LISTS DIRECTORY
    public function _ls(Request $request){
        $files=[];
        try{
            $files=array_slice(scandir(base_path('storage/app/uploads/'.$request->path)),2);
        }catch(\ErrorException $e){
            $files=[];
        }

        $data=(object)compact(
            'files'
        );

        return json_encode($data);
    }
    //  ALIAS OF _ls
    public function _dir(Request $request){
        return _ls($request);
    }

}
