<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Customer, ProspectProgress, ProjectType, ContractType, MarketingAvenue};

use App\Traits\HashController;

class ProspectsController extends Controller
{
	use HashController;

	/*
  	 *
	 * Validate that first contact date is on or before today (only if set)
	 * 
	 */
	private static function validateFirstContact($data){
		if (isset($data) && $data>today()->toDateString()){
			throw \Illuminate\Validation\ValidationException::withMessages([
			'first_contact' => ['La première date de contact doit être au plus tard à la date d\'aujourd\'hui.']
			]);
		}
	}

	/*
	 *
	 * Validate that all checked progresses have a progress date
	 *
	 */
	private static function validateProgressDates($userId,$ids,$dates,$created_at,&$result){

		foreach ($ids??[] as $id){
			/*
			*
			* WHEN A PROGRESS IS CHECKED, THERE MUST BE A DATE NO LATER THAN TODAY
			*
			*/
			$date=$dates[$id]??null;
			if($date==null){
				throw \Illuminate\Validation\ValidationException::withMessages([
					'progress_dates['.$id.']' => ['L\'état d\'avancement du projet doit avoir une date avancée.']
				]);
			}
			
			/*
			*
			* STORE CHECKED PROGRESSES
			*
			*/
			$result[$id]=[
				'user_id'=>$userId,
				'progress_date'=>$date,
				'prospect_created_at'=>$created_at
			];
		}

	}

	/*
	 *
	 * Store prospect data from request object
	 * 
	 */
	private static function storeProspectData($customer,$request){
		$customer->name=$request->name;
		$customer->surname=$request->surname;
		$customer->address=$request->address;
		$customer->city=$request->city;
		$customer->postal_code=$request->postal_code;
		$customer->email=$request->email;
		$customer->mobile=$request->mobile;
		$customer->telephone=$request->telephone;
		$customer->first_contact=$request->first_contact;
		$customer->marketing_avenue_id=$request->marketing_avenue_id;
		$customer->budget=$request->budget;
		$customer->project_type_id=$request->project_type_id;
		$customer->contract_type_id=$request->contract_type_id;
		$customer->comments=$request->comments;
		$customer->status=1;
	}

	//	ROUTER
	public function route($item=null){

		$mode;
		$common=$this->common();
		$modes=(object)[
			'index'=>null,
			'show'=>null
		];

		if($item=='create'){
			$mode='create';
		}elseif($item){
			$mode='show';
			$modes->show=$this->show($item);
		}else{
			$mode='index';
			$modes->index=$this->index();
		}

		$data=(object)compact(
			'mode',
			'common',
			'modes'
		);

		$this->hashAppend($data);

		return $data;

	}

	//	INTERNAL VERSION
	public function common(){
		$prospect_progresses=ProspectProgress::pluck('name','id');
		$prospect_progresses_by_order=ProspectProgress::pluck('id','order');
		$project_types=ProjectType::all();
		$contract_types=ContractType::all();
		$marketing_avenues=MarketingAvenue::all();

		$data=(object)compact(
			'prospect_progresses',
			'prospect_progresses_by_order',
			'project_types',
			'contract_types',
			'marketing_avenues'
		);

		$this->hashAppend($data);

		return $data;

	}
	//	API VERSION
	public function _common(Request $request){

		$data=$this->common();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	INTERNAL VERSION
  	public function index(){

		$page=Customer::where('status',1)
		->with(
			'contract_type',
			'prospect_progresses',
			'marketing_avenue'
		)
		->paginate(100);
		
		$current_page=$page->currentPage();
		$last_page=$page->lastPage();
		$prospects=$page->items();

		$data=(object)compact(
			'current_page',
			'last_page',
			'prospects'
		);

		$this->hashAppend($data);

		return $data;
	}
	//	API VERSION
	public function _index(Request $request){

		$data=$this->index();
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;
	}

	//	INTERNAL VERSION
	public function show($id){
		$prospect_id=$id;
		$prospect=Customer::with('prospect_progresses')->findOrFail($id);

		$data=(object)compact(
			'prospect_id',
			'prospect'
		);
	
		$this->hashAppend($data);

		return $data;
	}
	//	API VERSION
	public function _show(Request $request){

		$id=$request->id;
		$data=$this->show($id);
		$hash=$request->hash??null;

		$this->hashFilter($data,$hash);

		return $data;

	}

	//	API-ONLY
	public function _store(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;
		
		/*
		*
		* REGULAR VALIDATION
		*
		*/
		$request->validate([
			'data.name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
			'data.budget' => 'nullable|between:1,99999999.99',
			'data.first_contact' => 'nullable|date'
		]);
			
		//STRUCTURIZE DATA
		$request=(object)$request->data;
			
		/*
		 *
		 * CUSTOM VALIDATION
		 *
		 */

		// FIRST CONTACT VALIDATION (SOMETIMES)
		self::validateFirstContact($request->first_contact??null);

		//ARRAY TO STORE PROGRESSES TO ATTACH
		$requestProspectProgresses=[];

		//PROSPECT PROGRESS DATES VALIDATION
		self::validateProgressDates($userId,$request->prospect_progress_ids??[],$request->prospect_progress_dates??[],today()->toDateString(),$requestProspectProgresses);


		/*
		 *
		 * STORING PROSPECT DATA
		 *
		 */
		$customer=new Customer;
		$customer->user_id=$userId;

		self::storeProspectData($customer,$request);
		$customer->save();
		
		//ATTACH PROGRESS ARRAY
		$customer->prospect_progresses()->attach($requestProspectProgresses);

		
		//RETURN NEW PROSPECT URL AND ID
		$data=(object)[
			'url'=>'/prospects/'.$customer->id,
			'id'=>$customer->id
		];

		$data=json_encode($data);

		return $data;
		
	}

	//	API-ONLY
	public function _update(Request $request){

		//SET THE USER ID
		$userId=auth()->user()->id;

		/*
		 *
		 * REGULAR VALIDATION
		 *
		 */
		$request->validate([
		'data.name' => 'required|max:255|regex:/^[\pL\s\-]+$/u',
		'data.budget' => 'nullable|between:1,99999999.99',
		'data.first_contact' => 'nullable|date'
		]);

		$request=(object)$request->data;

		/*
		 *
		 * CUSTOM VALIDATION
		 *
		 */

		// FIRST CONTACT VALIDATION (SOMETIMES)
		self::validateFirstContact($request->first_contact??null);

		//ARRAY TO STORE PROGRESSES TO ATTACH
		$requestProspectProgresses=[];

		
		/*
		*
		* FIND PROSPECT
		* 
		*/
		$customer=Customer::findOrFail($request->id)->makeVisible('created_at');
		
		//PROSPECT PROGRESS DATES VALIDATION
		self::validateProgressDates($userId,$request->prospect_progress_ids??[],$request->prospect_progress_dates??[],$customer->created_at,$requestProspectProgresses);
		/*
		 *
		 * STORING PROSPECT DATA
		 *
		 */
		self::storeProspectData($customer,$request);
		

		$prospectProgressIds=ProspectProgress::all()->pluck('id');
		$requestProgressIds=$request->prospect_progress_ids??[];
		$customerCurrentProspectProgresses=$customer->prospect_progresses()->get();
		$customerCurrentProspectProgressIds=$customerCurrentProspectProgresses->pluck('id')->toArray();

		//loop through all available progresses in the database
		foreach ($prospectProgressIds as $prospectProgressId){

			//IF IN CURRENT IDs AND...
			if (in_array($prospectProgressId, $customerCurrentProspectProgressIds)){
				
				//GET PROGRESS LINK
				$progressPivot=$customerCurrentProspectProgresses->find($prospectProgressId)->pivot;

				//...IN REQUEST IDs AND...
				if (in_array($prospectProgressId, $requestProgressIds)){

				//GET PROGRESS DATES
				$customerProgressDate=$progressPivot->progress_date;
				$requestProgressDate=$requestProspectProgresses[$prospectProgressId]['progress_date'];

				//...PROGRESS DATE IS DIFFERENT THEN UPDATE
				if ($customerProgressDate!=$requestProgressDate){

					//DEACTIVATE OLD PIVOT
					$progressPivot->delete();

					//ATTACH NEW PIVOT
					$customer->prospect_progresses()->attach($prospectProgressId,$requestProspectProgresses[$prospectProgressId]);
				}

				}else{      
				//..NOT IN REQUEST IDs THEN SET STATUS 0 (DEACTIVATE)

				//DEACTIVATE OLD PIVOT
				$progressPivot->delete();

				}
			}else{

				
				//IF NOT IN CURRENT IDs and IN REQUEST IDs THEN SET ATTACH (ACTIVATE)
				if (in_array($prospectProgressId, $requestProgressIds)){

				$customer->prospect_progresses()->attach($prospectProgressId,$requestProspectProgresses[$prospectProgressId]);

				}else{
				//IF NOT IN CURRENT IDs and NOT IN REQUEST IDs THEN DO NOTHING (inactive - still inactive)
				
				//do nothing

				}


			}

		}

		$customer->save();

		//RETURN PROSPECT URL AND ID
		$data=(object)[
			'url'=>'/prospects/'.$customer->id,
			'id'=>$customer->id
		];

		$data=json_encode($data);

		return $data;
	}

	//	API-ONLY
	public function _destroy(Request $request){
		try{
			Customer::findOrFail($request->id)->forceDelete();
		}catch(\Illuminate\Database\QueryException $e){
			Customer::findOrFail($request->id)->delete();
		}
		return json_encode((object)['data'=>'deleted '.$request->id]);
	}

	//	API-ONLY
	public function _search(Request $request){

		$queries=$request->q;

		if (!$queries || count($queries)<1) return;

		$first=strtolower($queries[0]);

		$result=Customer::where('name','like',"%$first%")
		->orWhere('surname','like',"%$first%");

		$result=array_reduce(array_splice($queries,1),function($previous,$term){
			return $previous->where('name','like',"%$term%")->orWhere('surname','like',"%$term%");
		},$result);

		$result=$result->limit(10)->get()->each(function($customer){
			$customer->makeVisible('status');
		});;

		$data=(object)compact(
			'result'
		);

		return json_encode($data);

	}

}
