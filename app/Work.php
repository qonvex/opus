<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Work extends Model
{

    use SoftDeletes;

    protected $hidden=['pivot','user_id','status','created_at','updated_at'];
    
    public function delete(){
        $this->prices()->delete();
        $this->project_price_assignments()->delete();

        return parent::delete();
    }

    public function getCompanyPrice($company_id){
        $price_link=$this
        ->prices
        ->where('company_id',$company_id)
        ->where('status',1)
        ->first();
        return $price_link?$price_link->price_per_unit:null;
    }

    public function prices()
    {
        return $this
            ->hasMany(
                'App\CompanyWorkPrice'
            );
            
    }

    public function project_price_assignments(){
        return $this
            ->hasMany(
                'App\ProjectPriceAssignment'
            );
    }

    public function work_category(){

        return $this
            ->belongsTo(
                'App\WorkCategory'
            );

	}

    public function companies(){

        return $this
        ->belongsToMany(
            'App\Company',
            'company_work_prices',
            'work_id',
            'company_id'
        );

    }
}
