<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProspectProgress extends Model
{
    //

	public function customers(){

		return $this
			->belongsToMany(
				'App\Customer',
				'customer_prospect_progresses',
				'prospect_progress_id',
				'customer_id'
			)
			->withTimestamps();

	}

}
