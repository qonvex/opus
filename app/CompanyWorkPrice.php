<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyWorkPrice extends Model
{
    protected $hidden=['user_id','status','created_at','updated_at'];
}
