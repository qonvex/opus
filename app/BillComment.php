<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillComment extends Model
{

	protected $hidden=['user_id','created_at','updated_at'];

	public function bill(){

		return $this->belongsTo('App\Bill');
		
	}

	public function company(){

		return $this->belongsTo('App\Company');

	}
	
	public function work_category(){

		return $this->belongsTo('App\WorkCategory');

	}

	public function prices(){

		return $this->hasMany('App\BillPrice');

	}

}
