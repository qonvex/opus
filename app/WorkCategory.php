<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkCategory extends Model
{
	
	protected $hidden=['pivot','created_at','updated_at','status'];

	public static function getAvailable(){
		$workCategoryIds=self::pluck('id')->toArray();
		$workCategoryAssignmentEntries=CompanyWorkCategoryAssignment::where('status',1)->pluck('work_category_id')->toArray();
		$entryCountById=array_count_values($workCategoryAssignmentEntries);
		$available=[];

		foreach ($workCategoryIds as $workCategoryId){
			if(($entryCountById[$workCategoryId]??0)<12){
				$available[]=$workCategoryId;
			}
		}

		return self::findMany($available);
	}

	public static function isAvailable($id){
		$entryCount=CompanyWorkCategoryAssignment::where('work_category_id', $id)->where('status',1)->count();
		return $entryCount<12?true:false;
	}
    
    public function companies(){

    	return $this
    		->belongsToMany(
    			'App\Company',
    			'company_work_category_assignments',
    			'work_category_id',
    			'company_id'
			);

	}
	
	public function works(){

		return $this
			->hasMany(
				'App\Work'
			);

	}

}
