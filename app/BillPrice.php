<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillPrice extends Model
{
	
	public function bill(){

		return $this->belongsTo('App\Bill');

	}

	public function bill_comment(){

		return $this->belongsTo('App\BillComment');
	}

	public function work(){

		return $this->belongsTo('App\Work');
		
	}
	
}
