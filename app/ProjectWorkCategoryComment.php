<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectWorkCategoryComment extends Model
{
	
	use SoftDeletes;

	public function delete(){
		$this->prices()->delete();

		return parent::delete();
	}	
	
	public function prices()
	{

		return $this->hasMany('App\ProjectPriceAssignment', 'work_comment_id');

	}

}
