<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

	use SoftDeletes;

	protected $hidden=['pivot'];

	public function delete(){
		$this->work_category_assignments()->delete();
		$this->project_work_category_comments()->delete();
		$this->company_work_prices()->delete();
		$this->project_price_assignments()->delete();

		return parent::delete();
	}

	public function project_price_assignments(){
		return $this
			->hasMany(
				'App\ProjectPriceAssignment'
			);
	}

	public function work_category_assignments(){
		return $this
			->hasMany(
				'App\CompanyWorkCategoryAssignment'
			);
	}

	public function project_work_category_comments(){
		return $this
			->hasMany(
				'App\ProjectWorkCategoryComment'
			);
	}

	public function company_work_prices(){
		return $this
			->hasMany(
				'App\CompanyWorkPrice'
			);
	}

	public function work_categories(){

		return $this
			->belongsToMany(
				'App\WorkCategory',
				'company_work_category_assignments',
				'company_id',
				'work_category_id'
			)
			->withPivot([
				'user_id',
				'status'
			])
			->withTimestamps();

	}

	public function prices(){

    	return $this
    		->belongsToMany(
    			'App\Work',
    			'company_work_prices',
    			'company_id',
    			'work_id'
			)
			->withPivot([
                'price_per_unit',
				'user_id',
				'status'
			])
    		->withTimestamps();

    }

}
