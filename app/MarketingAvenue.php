<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingAvenue extends Model
{
    //
	public function getFullNameAttribute(){
		return $this->name. ' (' . $this->description . ')';
	}

}
