<?php

namespace App\Traits;

trait HashController{

	public function hashFilter(&$data,$hash){

		$jsonData=json_encode($data);
		$dataHash=$data->hash;

		if($dataHash===$hash){
			$data=(object)[
				'data_state'=>'old'
			];
		}else{
			$data->data_state='new';
		}

		$data=json_encode($data);

	}

	public function hashAppend(&$data){

		$jsonData=json_encode($data);
		$dataHash=hash('md4',$jsonData);
		
		$data->hash=$dataHash;

	}

}