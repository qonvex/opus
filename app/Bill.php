<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
	
	protected $hidden=['user_id','status','updated_at'];

	public function project(){

		return $this->belongsTo('App\Project');
	}

	public function customer(){

		return $this->belongsTo('App\Customer');

	}

	public function comments(){

		return $this->hasMany('App\BillComment');

	}
	
	public function prices(){

		return $this->hasMany('App\BillPrice');

	}


}
