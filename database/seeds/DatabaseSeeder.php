<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

    	//seed some marketing avenues 
    	// DB::table('marketing_avenues')->insert([
    	// 	'name'=>'Salon',
    	// 	'description'=>'Salon'
    	// ]);

    	//seed some prospect progresses
		// DB::table('prospect_progresses')->insert([
		// 	'name'=>'R.A.F.',
		// 	'order'=>1
		// ]);
		
    	//seed some project progresses
		// DB::table('project_progresses')->insert([
		// 	'name'=>'Commencé',
		// 	'order'=>1
    	// ]);

        //seed some work categories
        // DB::table('work_categories')->insert([
        //     'name'=>'Administratif',
        //     'description'=>''
        // ]);
		
		//seed some project types
		// DB::table('project_types')->insert([
		// 	'name'=>'Rénovation'
		// ]);

		//seed some contract types
		// DB::table('contract_types')->insert([
		// 	'name'=>'Contractant'
		// ]);

    	//seed some customers 
    	// DB::table('customers')->insert([

		// ]);

    }
}
