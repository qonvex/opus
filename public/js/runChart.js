// START LINE CHART
const CHART = document.getElementById('lineChart');

Chart.defaults.global.defaultFontSize = 10;
Chart.defaults.global.defaultFontColor = '#777';

let lineChart = new Chart(CHART, {
	type:'line',
	data: {
		labels:['0','2','4','6','8','10'],
		datasets:[{
			label: 'CA',
			fill: false,
			lineTension: 0.0,
			borderColor:"rgba(75,192,192,1)",
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle:'miter',
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "#fff",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [0.7,6,7,2,10,7,4,5,4],
		},
		{
			label: 'Objectif CA',
			fill: false,
			lineTension: 0.0,
			borderColor:"rgba(0,0,0,1)",
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle:'miter',
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "rgba(75,192,192,0.4)",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [8,7.9,7.8,7.7,7.6,6],
		}],
	},

	options:{
		legend:{
			display: true,
		},
		scales: {
            yAxes: [{
                 ticks: {
                 	max: 10,
		            min: 0,
					stepSize: 5,
                 	suggestedMin: 5,
	                suggestedMax: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return value + 'k €';
                    }
                }
            }]
        }
	}
});

const CHART2 = document.getElementById('lineChart2');

let lineChart2 = new Chart(CHART2, {
	type:'line',
	data: {
		labels:['0','2','4','6','8','10'],
		datasets:[{
			label: 'CA',
			fill: false,
			lineTension: 0.0,
			backgrounColor:"rgba(75,192,192,0.4)",
			borderColor:"rgba(75,192,192,1)",
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle:'miter',
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "#fff",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [3,6,7,2,10,7,4,5,4],
		},
		{
			label: 'Objectif CA',
			fill: false,
			lineTension: 0.0,
			backgrounColor:"rgba(75,192,192,0.4)",
			borderColor:"rgba(0,0,0,1)",
			borderCapStyle: 'butt',
			borderDash: [],
			borderDashOffset: 0.0,
			borderJoinStyle:'miter',
			pointBorderColor: "rgba(75,192,192,1)",
			pointBackgroundColor: "rgba(75,192,192,0.4)",
			pointBorderWidth: 1,
			pointHoverRadius: 5,
			pointHoverBackgroundColor: "rgba(220,220,220,1)",
			pointHoverBorderWidth: 2,
			pointRadius: 1,
			pointHitRadius: 10,
			data: [8,7.9,7.8,7.7,7.6,6],
		}],


	}, 

	options:{
		legend:{
			display: true,
		},
		scales: {
            yAxes: [{
                 ticks: {
              //  using  max: 10,  and
		            // min: 0, if the data exceeds to 10 
		            // the line in the graph which where the point it is exceeded will not be able to show.
					stepSize: 5,
                 	suggestedMin: 5,
	                suggestedMax: 10,
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return value + 'k €';
                    }
                }
            }]
        }
	}


});
// END LINE CHART

