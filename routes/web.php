<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/logout','Auth\LoginController@logout')->name('get.logout');

//	APP VERSION CHECK
Route::get('/version','PagesController@version');

//	LINKS API
Route::get('/api/links','PagesController@_links')->name('api.links');

Route::group(['middleware' => ['auth']], function() {

	//	CHANGE PASSWORD
	Route::get('/password','Auth\ChangePasswordController@showChangePasswordForm')->name('change_password');
	Route::get('/password/success','Auth\ChangePasswordController@success')->name('password.success');
	Route::post('/password','Auth\ChangePasswordController@changePassword')->name('change');

	//	PROSPECTS API
	Route::get('/api/prospects/common','ProspectsController@_common')->name('api.prospects.common');
	Route::get('/api/prospects/index','ProspectsController@_index')->name('api.prospects.index');
	Route::get('/api/prospects/show','ProspectsController@_show')->name('api.prospects.show');
	Route::get('/api/prospects/create',function(){return json_encode(['data'=>'data']);})->name('api.prospects.create');

		Route::get('/api/prospects/search','ProspectsController@_search')->name('api.prospects.search');

		Route::post('/api/prospects','ProspectsController@_store')->name('api.prospects.store');
		Route::patch('/api/prospects','ProspectsController@_update')->name('api.prospects.update');
		Route::delete('/api/prospects','ProspectsController@_destroy')->name('api.prospects.delete');

	//	CLIENTS API
	Route::get('/api/clients/common','ClientsController@_common')->name('api.clients.common');
	Route::get('/api/clients/index','ClientsController@_index')->name('api.clients.index');
	Route::get('/api/clients/show','ClientsController@_show')->name('api.clients.show');
	Route::get('/api/clients/create',function(){return json_encode(['data'=>'data']);})->name('api.clients.create');

		Route::post('/api/clients','ClientsController@_store')->name('api.clients.store');
		Route::patch('/api/clients','ClientsController@_update')->name('api.clients.update');
		Route::delete('/api/clients','ClientsController@_destroy')->name('api.clients.delete');

	//	COMPANIES API
	Route::get('/api/companies/common','CompaniesController@_common')->name('api.companies.common');
	Route::get('/api/companies/index','CompaniesController@_index')->name('api.companies.index');
	Route::get('/api/companies/show','CompaniesController@_show')->name('api.companies.show');
	Route::get('/api/companies/create','CompaniesController@_create')->name('api.companies.create');
	
		Route::post('/api/companies','CompaniesController@_store')->name('api.companies.store');
		Route::patch('/api/companies','CompaniesController@_update')->name('api.companies.update');
		Route::delete('/api/companies','CompaniesController@_destroy')->name('api.companies.delete');

	//	WORK CATEGORIES API
	Route::get('/api/work_categories/common','WorkCategoriesController@_common')->name('api.work_categories.common');
	Route::get('/api/work_categories/index','WorkCategoriesController@_index')->name('api.work_categories.index');
	Route::get('/api/work_categories/show','WorkCategoriesController@_show')->name('api.work_categories.show');
	Route::get('/api/work_categories/create','WorkCategoriesController@_create')->name('api.work_categories.create');
	
		Route::post('/api/work_categories','WorkCategoriesController@_store')->name('api.work_categories.store');
		Route::patch('/api/work_categories','WorkCategoriesController@_update')->name('api.work_categories.update');

	//	PROJECTS API
	Route::get('/api/projects/common','ProjectsController@_common')->name('api.projects.common');
	Route::get('/api/projects/index','ProjectsController@_index')->name('api.projects.index');
	Route::get('/api/projects/show','ProjectsController@_show')->name('api.projects.show');
	Route::get('/api/projects/create','ProjectsController@_create')->name('api.projects.create');
	
		Route::post('/api/projects','ProjectsController@_store')->name('api.projects.store');
		Route::patch('/api/projects','ProjectsController@_update')->name('api.projects.update');
		Route::delete('/api/projects','ProjectsController@_destroy')->name('api.projects.delete');

	//	BILLS API
	Route::get('/api/bills/common','BillsController@_common')->name('api.bills.common');
	// Route::get('/api/bills/index','BillsController@_index')->name('api.bills.index');
	Route::get('/api/bills/show','BillsController@_show')->name('api.bills.show');
	
		Route::post('/api/bills','BillsController@_store')->name('api.bills.store');
	
	//	CONFIG API
	Route::get('/api/config/common','ConfigController@_common')->name('api.config.common');
	Route::get('/api/config/index','ConfigController@_index')->name('api.config.index');
	Route::get('/api/config/show','ConfigController@_show')->name('api.config.show');
	Route::get('/api/config/create','ConfigController@_create')->name('api.config.create');
	
		Route::post('/api/config','ConfigController@_store')->name('api.config.store');
		Route::patch('/api/config','ConfigController@_update')->name('api.config.update');
		Route::delete('/api/config','ConfigController@_destroy')->name('api.config.delete');
	
	//	WORKS API
	Route::get('/api/works/show','WorksController@_show')->name('api.works.show');

		Route::post('/api/works','WorksController@_store')->name('api.works.store');
		Route::patch('/api/works','WorksController@_update')->name('api.works.update');
		Route::delete('/api/works','WorksController@_destroy')->name('api.works.delete');

	//	PAGES API
	Route::get('/api/pages/dashboard','PagesController@_dashboard')->name('api.dashboard');
	Route::get('/api/pages/prospects','PagesController@_prospects')->name('api.prospects');
	Route::get('/api/pages/clients','PagesController@_clients')->name('api.clients');
	Route::get('/api/pages/companies','PagesController@_companies')->name('api.companies');
	Route::get('/api/pages/work_categories','PagesController@_work_categories')->name('api.work_categories');
	Route::get('/api/pages/projects','PagesController@_projects')->name('api.projects');
	Route::get('/api/pages/config','PagesController@_config')->name('api.config');
	// Route::get('/api/pages/bills','PagesController@_bills')->name('api.bills');

		Route::get('/api/search','PagesController@_search')->name('api.search');

	//	COMMON API

	//	FILES API
	Route::get('/api/files/dir','FilesController@_ls')->name('api.files.dir');
	Route::get('/api/files/fetch64','FilesController@_fetch64')->name('api.files.fetch64');
	Route::get('/api/files/fetch','FilesController@_fetch')->name('api.files.fetch');
	Route::get('/api/files/resource','FilesController@_resource')->name('api.files.resource');
	Route::delete('/api/files/delete','FilesController@_delete')->name('api.files.delete');
	Route::post('/api/files/upload','FilesController@_upload')->name('api.files.upload');

	//	APP LAUNCHER 
	Route::get('/{resource?}/{item?}/{item2?}', 'PagesController@app')->name('app');
	

});


