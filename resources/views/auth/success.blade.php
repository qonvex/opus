<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>

		@include('inc.header')

	</head>

	<body>

    <div class="login-page container-fluid">
        <!-- START LOG IN BOX -->
        <div class="row">
            <div class="login-box">
                <div class="row">
                    <!-- LOGO -->
                    <div class="login-logo col-md-5 col-sm-12 col-xs-12">
                        <div class="logo-img">
                            <img class="img-responsive" src="{{asset('img/logo.jpg')}}">
                        </div>
                    </div>

                    <!-- LOGIN FORM -->
                    <form class="login-form col-md-7 col-sm-12 col-xs-12" method="GET" action="/">

                        <h6>Succès</h6>

                        <p>Votre mot de passe a été changé.</p>

                        <input class="btn btn-md login btn-blue btn-block" type="submit" value="Accueil">
                    </form>
                    <!-- END LOG IN FORM -->
                </div>	
            </div>
        </div>
        <!-- END LOG IN BOX -->
    </div>

	@include('inc.scripts')
	
	</body>
</html>
