<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>

		@include('inc.header')

	</head>

	<body>

    <div class="login-page container-fluid">
        <!-- START LOG IN BOX -->
        <div class="row">
            <div class="login-box">
                <div class="row">
                    <!-- LOGO -->
                    <div class="login-logo col-md-5 col-sm-12 col-xs-12">
                        <div class="logo-img">
                            <img class="img-responsive" src="{{asset('img/logo.jpg')}}">
                        </div>
                    </div>

                    <!-- LOGIN FORM -->
                    <form class="login-form col-md-7 col-sm-12 col-xs-12" method="POST" action="{{ route('change') }}">

                        @csrf
                        
                        @if(isset($error))
                            <div class="alert alert-danger" >{{ $error }}</div>
                        @endif

                        <h6>Changer le mot de passe</h6>

                        <div class="form-group">
                            
                                <input class="form-control input-border-bottom login-input" type="password" placeholder="Actuel" id="password" name="current_password" required autofocus>
                                <input class="form-control input-border-bottom login-input" type="password" placeholder="Nouveau" id="password" name="new_password" required autofocus>
                                <input class="form-control input-border-bottom login-input" type="password" placeholder="Confirmer" id="password" name="confirm_password" required>

                        </div>

                        <input class="btn btn-md login btn-blue btn-block" type="submit" name="change" value="Soumettre">
                    </form>
                    <!-- END LOG IN FORM -->
                </div>	
            </div>
        </div>
        <!-- END LOG IN BOX -->
    </div>

	@include('inc.scripts')
	
	</body>
</html>
