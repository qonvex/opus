@extends('layouts.app')

@section('content')

    <div class="login-page container-fluid">
        <!-- START LOG IN BOX -->
        <div class="row">
            <div class="login-box">
                <div class="row">
                    <!-- LOGO -->
                    <div class="login-logo col-md-5 col-sm-12 col-xs-12">
                        <div class="logo-img">
                            <img class="img-responsive" src="{{asset('img/logo.jpg')}}">
                        </div>
                    </div>

                    <!-- LOGIN FORM -->
                    <form class="login-form col-md-7 col-sm-12 col-xs-12" method="POST" action="{{ route('login') }}">

                        @csrf

                        <h6>Connexion</h6>

                        <div class="form-group">
                            
                                <input class="form-control input-border-bottom login-input" type="text" placeholder="Nom d'utilisateur" id="username" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                <input class="form-control input-border-bottom login-input" type="password" placeholder="Mot de passe" id="password" name="password" required autocomplete="current-password">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <input class="btn btn-md login btn-blue btn-block" type="submit" name="login" value="Connexion">
                    </form>
                    <!-- END LOG IN FORM -->
                </div>	
            </div>
        </div>
        <!-- END LOG IN BOX -->
    </div>

@endsection