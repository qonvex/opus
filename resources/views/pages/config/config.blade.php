@extends('layouts.app')

@section('api')

@endsection

@section('content')

	<h1>This is the config page</h1>

	@foreach ($data as $table)

		<h3>{{ $table['label'] }} <button class="btn btn-primary" onclick="ajax('GET','{{ route('configs.create',[$table['table']]) }}')">Add</button></h3>


		@foreach ($table['content'] as $row)
			
			<div class="card card-body" onmouseover="this.style.background='#FACF'" onmouseleave="this.style.background='#FAC0'" onclick="ajax('GET','{{route('configs.edit',[$table['table'],$row['id']])}}')">
				<pre>
					{{ print_r($row) }}
				</pre>
			</div>

		@endforeach

	@endforeach

@endsection