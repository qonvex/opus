@extends('layouts.app')

@php
	//define routes

	$updateProspectURL=route('prospects.update',':id');
	$indexProspectURL=route('prospects.index');

@endphp

@section('content')

<!-- START CONTENT -->
<div class="page-content response-content">
    <!-- START VUE PROSPECT -->
    <div class="vue-prospect form">
        <div class="title-label row">
            <div class="col-9">
                <h2>Vue Prospect</h2>
            </div>
            <div class="col-3">
                <div class="prospect-btn-circle">
                    <div class="btn-circle">
                        <button class="btn-sm btn btn-circle-white">
                        <a href="" id='edit-link' method='get'><img class="img-responsive rounded" src="{{asset('img/edit2.png')}}"></a>
                        </button>
                    </div>
                    <div class="btn-circle">
                        <button class="btn-sm btn btn-circle-blue">
                            <a href="" id='index-link' class='ajax-link' method='get'><img class="img-responsive rounded" src="{{asset('img/arrow-left2.png')}}"></a>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- START FORM -->
        <form class="from-group row prospect-form">
            @csrf
            <!-- START LEFT COLUMN FORM -->
            <div class="col-md-7">
                <!-- NOM AND PRENOM -->
                <div class="row">
                    <div class="col-md-6">
                        <label class="label" for="nom">Nom</label>
                        <input disabled type="text" required class="input-text input-text input-border-bottom form-control" name="name">
                    </div>
                    <div class=" col-md-6">
                        <label class="label" for="nom">Prénom</label>
                        <input disabled type="text" class="input-text input-text input-border-bottom form-control" name="surname">
                    </div>
                </div>
                <!-- ADRESSE AND EMAIL -->
                <div class="row">
                        <div class=" col-md-12">
                        <label class="label" for="adresse">Adresse</label>
                        <input disabled type="text" class="input-text input-border-bottom form-control" name="address">
                        <label class="label" for="email">Email</label>
                        <input disabled type="email" class="input-text input-border-bottom form-control" name="email">
                    </div>
                </div>
                <!-- TELEPHONE MOBILE AND TELEPHONE FIXE -->
                <div class="row">
                    <div class="  col-md-6">
                        <label class="label" for="telephone-mobile">Téléphone mobile </label>
                        <input disabled type="text" class="input-text input-border-bottom form-control" name="mobile">
                    </div>
                    <div class=" col-md-6">
                        <label class="label" for="telephone-fixe">Téléphone fixe</label>
                        <input disabled type="text" class="input-text input-border-bottom form-control" name="telephone">
                    </div>
                </div>
                <!-- TYPE DE TRAVAUX -->
                <div class="row">
                        <div class=" col-md-12">
                        <label class="label" for="travaux">Type de travaux</label>
                        <select disabled class=" input-text input-border-bottom form-control" name="project_type_id">
                        </select>
                    </div>
                </div>
                <!-- TYPE -->
                <div class="row">
                    <div class=" col-md-12">
                        <label class="label" for="type">Type</label>
                        <select disabled class=" input-text input-border-bottom form-control" name="contract_type_id">
                        </select>
                    </div>
                </div>
                <!-- DESCRIPTION -->
                <div class="description row">
                    <div class=" col-md-12">
                        <label class="label" for="description">Description</label>
                        <textarea disabled class="input-text input-border-bottom form-control" rows="3" name="comments"></textarea>
                    </div>
                </div>
            </div>
            <!-- END  LEFT COLUMN FORM-->

            <!-- START RIGHT COLUMN FORM -->
            <div class="col-md-5">
                <!-- DATE PRISE CONTACT -->
                <div class="row">
                    <div class=" col-md-12">
                        <label class="label" for="date">Date prise Contact</label>
                        <input disabled type="date" class="input-text input-border-bottom form-control" name="first_contact">
                    </div>
                </div>
                <!-- ORIGINE -->
                <div class="row">
                    <div class=" col-md-12">
                        <label class="label" for="origine">Origine</label>
                        <select disabled class="form-control input-text input-border-bottom" name="marketing_avenue_id">
                        </select>
                    </div>
                </div>
                <!-- BUDGET -->
                <div class="row">
                    <div class="col-md-12">
                        <label class="label">Budget</label>
                        <div class="input-group input-border-bottom">
                        <div>
                            <span class="input-group-text input-border-none">€</span>
                        </div>
                        <input disabled type="text" class="currency input-text input-border-none form-control" name="budget">
                        </div>
                    </div>
                </div>
                <!-- HISTORIQUE RDV -->
                <div class="row">
                    <div class="col-md-12 progress-container">
                        <label class="label">Historique RDV</label>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM -->
    </div>
    <!-- END VUE PROSPECT -->
</div>
<!-- END CONTENT -->

@endsection

@section('scripts')

<script class="page-script">

    /*
     * Retrieve data from the server.
     */
    parseResponse("<?php echo addslashes(htmlspecialchars($data, ENT_NOQUOTES)); ?>");

    /*
     * ASSET DECLARATIONS
     */
    var progressLabel;
    var progressRow;
    var nameField;
    var surnameField;
    var addressField;
    var emailField;
    var mobileField;
    var telephoneField;
    var projectTypeIdField;
    var contractTypeIdField;
    var commentsField;
    var firstContactField;
    var marketingAvenueIdField;
    var budgetField;
    var progressContainer;
    var allFields=[];

    /*
     * ASSET LOADING HANDLER
     */
    function loadAssets(){
        progressLabel=$(`
        <label class="label">Historique RDV</label>
        `);
        progressRow=$(`
        <div class="progress-row row">
            <div class="col-md-6">
                <label class="checkbox-inline input-text">
                        <input disabled class="input-text" type="checkbox" required="required">
                </label>
            </div>
            <div class="col-md-6">
                <input disabled type="date" class="input-text input-border-bottom form-control">
            </div>
        </div>
        `);
        nameField=$("[name='name']");
        surnameField=$("[name='surname']");
        addressField=$("[name='address']");
        emailField=$("[name='email']");
        mobileField=$("[name='mobile']");
        telephoneField=$("[name='telephone']");
        projectTypeIdField=$("[name='project_type_id']");
        contractTypeIdField=$("[name='contract_type_id']");
        commentsField=$("[name='comments']");
        firstContactField=$("[name='first_contact']");
        marketingAvenueIdField=$("[name='marketing_avenue_id']");
        budgetField=$("[name='budget']");
        progressContainer=$(".progress-container");
        allFields=[
            nameField,
            surnameField,
            addressField,
            emailField,
            mobileField,
            telephoneField,
            projectTypeIdField,
            contractTypeIdField,
            commentsField,
            firstContactField,
            marketingAvenueIdField,
            budgetField
        ];
    }

    /*
     * PAGE BUILDING HANDLER
     */
    function buildPage(){
        let prospect=data.prospect;
        //links
        let indexLink='{{$indexProspectURL}}';

        $('#index-link').attr('href',indexLink);

        progressContainer.html('');
        progressContainer.append(progressLabel.clone(0));
            
        let progresses=[];
        let progressDates=[];
        prospect.prospect_progresses.forEach(progress=>{
            progresses[progress.id]=progress.name; 
            progressDates[progress.id]=progress.pivot.progress_date; 
        });

        for (let key in data.prospect_progresses_by_order){
            if (!data.prospect_progresses_by_order.hasOwnProperty(key)) continue;

            let prospect_progress_id = data.prospect_progresses_by_order[key];
            let prospect_progress_name=data.prospect_progresses[prospect_progress_id];

            let progressRowItem=progressRow.clone(0);

            progressRowItem.find('label').append(prospect_progress_name);
            allFields.push(progressRowItem.find("[type='checkbox']").attr('name','prospect_progress_ids['+key+']').val(key));
            allFields.push(progressRowItem.find("[type='date']").attr('name','prospect_progress_dates['+key+']'));

            if (prospect_progress_id in progresses){
                progressRowItem.find("[type='checkbox']").attr('checked',true);
                progressRowItem.find("[type='date']").val(progressDates[prospect_progress_id])
            }else{
                progressRowItem.addClass('text-muted');
                progressRowItem.find("[type='date']").hide();
            }

            progressContainer.append(progressRowItem);

        }

        for (let key in data.project_types){
            let option=$('<option>',{
                value:key
            });
            option.html(data.project_types[key]);

            if (key==prospect.project_type_id){
                option.attr('selected',true);
            }

            projectTypeIdField.append(option);
        }

        for (let key in data.contract_types){
            let option=$('<option>',{
                value:key
            });
            option.html(data.contract_types[key]);

            if (key==prospect.contract_type_id){
                option.attr('selected',true);
            }

            contractTypeIdField.append(option);
        }

        for (let key in data.marketing_avenues){
            let option=$('<option>',{
                value:key
            });
            option.html(data.marketing_avenues[key]);

            if (key==prospect.marketing_avenue_id){
                option.attr('selected',true);
            }

            marketingAvenueIdField.append(option);
        }

        nameField.val(prospect.name);
        surnameField.val(prospect.surname);
        addressField.val(prospect.address);
        emailField.val(prospect.email);
        mobileField.val(prospect.mobile);
        telephoneField.val(prospect.telephone);
        commentsField.html(prospect.comments);
        budgetField.val(prospect.budget);
        
        projectTypeIdField.val(prospect.project_type_id);
        contractTypeIdField.val(prospect.contract_type_id);
        marketingAvenueIdField;
    }

    /*
     * ENABLE FIELD EDITING
     */
    $(document).on('click','#edit-link',function(e){
        e.stopImmediatePropagation();
        e.preventDefault();
        enableEdit();
    });
    function enableEdit(){
        allFields.forEach(field=>{
            field.removeAttr('disabled');
            field.show();
        })
        progressContainer.find('.progress-row').removeClass('text-muted');
        $('#edit-link img').attr('src','{{asset('img/save_icon.png')}}');
        $('#index-link img').attr('src','{{asset('img/close_icon.png')}}');

        $('#index-link').removeClass('ajax-link');
        $(document).off('click','#edit-link');
        $(document).on('click','#edit-link',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            saveChanges();
        });
        $(document).on('click','#index-link',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            discardChanges();
        })


    }

    /*
     * DISABLE FIELD EDITING
     */
    function disableEdit(){
        allFields.forEach(field=>{
            field.attr('disabled',true);
            field.show();
        });
        $('#edit-link img').attr('src','{{asset('img/edit2.png')}}');
        $('#index-link img').attr('src','{{asset('img/arrow-left2.png')}}');

        $('#index-link').addClass('ajax-link');
        $(document).off('click','#edit-link');
        $(document).on('click','#edit-link',function(e){
            e.stopImmediatePropagation();
            e.preventDefault();
            enableEdit();
        });
    }

    /*
     * SAVE CHANGES
     *
     */
    function saveChanges(){
        $('#edit-link img').attr('src','{{asset('img/loading.gif')}}');

        let formData=$('.prospect-form').serialize();
        let url='{{$updateProspectURL}}';
        url=url.replace(':id',data.prospect.id);
        $.put(
			url,
			formData,
			function(response){
                data=JSON.parse(response);
                disableEdit();
                buildPage();
        	}
		).fail(function(json){
			console.log(json);
        });

    }

    /*
     * DISCARD CHANGES
     */
    function discardChanges(){
        disableEdit();
        buildPage();
    }
    
    
</script>

@endsection