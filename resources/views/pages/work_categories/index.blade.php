@extends('layouts.app')

@section('content')

<button id='add-work' type="button" class="btn btn-primary ml-auto">Add Work</button>

<div id="selector" class='col'>

    @include('inc.forms.dropdown',['options'=>App\WorkCategory::all(), 'optionLabel'=>'name', 'name'=>'marketing_avenue_id', 'label'=>'Category'])

</div>

<div id="table">

    <table class='table'>
        <thead>
            <tr>
                <th>Work</th>
                <th>Unit</th>
                <th>Company 1</th>
                <th>Company 2</th>
                <th>Company 3</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>

@endsection

@section('scripts')

<script>

    function buildTable(json){
        let data=JSON.parse(json);


        /*
         *
         * BUILD HEADER
         *
         */

        let headRow=$('<tr/>');
        let nameHead=$('<th>Work</th>');
        let unitHead=$('<th>Unit</th>');

        nameHead.appendTo(headRow);
            unitHead.appendTo(headRow);

        data.companies.forEach(company => {
            let companyHead=$('<th>'+company.name+'</th>');

            companyHead.appendTo(headRow);
        });

        $('#table thead').html(headRow);



        /*
         *
         * BUILD BODY
         *
         */
        let tableBody=$('#table tbody');

        tableBody.html('');

        data.works.forEach(work => {

            let row=$('<tr/>',{
                class:'clickable-row',
                name:work.id
            });
            row.append('<td>'+work.name+'</td>');
            row.append('<td>'+work.unit+'</td>');

            data.companies.forEach(company => {
                let companyPrice=work.prices[company.id]||'';

                row.append('<td>'+companyPrice+'</td>');
            });

            tableBody.append(row);
        });

    }

    function updateTable(){
        let categorySelected=$('#selector option:selected').val();
        jQuery.ajax({
            type: 'GET',
            url: '/work_categories/'+categorySelected,
            success: function (data) {
                buildTable(data);
            },
            error: function(data) { 
                console.log(data);
            }
        });
    }

    function addWork(){
        let url='{{route('work_categories.create_work',':id')}}';
        let categoryId=$('#selector option:selected').val();

        url=url.replace(':id',categoryId);
        ajax('GET',url,'Add Work');
    }

    function editWork(id){
        let url='{{route('work_categories.edit_work',':id')}}';
        url=url.replace(':id',id);

        ajax('GET',url,'Edit Work');
    }
    
    $(document).on('change','#selector',function(){
        updateTable();
    });

    $(document).on('click','#add-work',function(){
        if($('#selector option:selected').prop('disabled')!=true)
        addWork();
    })
    
    $(document).on('click','.clickable-row',function(){
        editWork($(this).attr('name'));
    })
    

</script>

@endsection