<form
    id='create-form'>
			
    {{-- CROSS-SITE PROTECTION --}}
    @csrf

    <!-- CATEGORY -->
    @include('inc.forms.dropdown', ['options'=>App\WorkCategory::all(), 'optionLabel'=>'name', 'name'=>'category_id', 'label'=>'Category', 'base'=>$workCategoryId, 'class'=>'modal-selector'])

    <!-- NAME -->
    @include('inc.forms.field',['name'=>'name', 'attributes'=>'required', 'label'=>'Name'])

    <!-- UNIT -->
    @include('inc.forms.field',['name'=>'unit', 'attributes'=>'required', 'label'=>'Unit'])

    <div id="companies-container">

    </div>

    <button id='create-form-submit' type='submit' class='btn btn-primary'>Add</button>

</form>

<script>


    function submitStoreForm(){
        let formData=$('#create-form').serialize();
        $('#create-form').trigger("reset");
        $.post(
			'{{ route('work_categories.store_work')}}',
			formData,
			function(json){
				response=JSON.parse(json);
				clearModal();
				sendMessage(response.type,response.message);
				updateTable();
        	}
		).fail(function(xhr, status, error){
			console.log(xhr);
        });
    }

    function buildFields(json){
        let data=JSON.parse(json);

        let fieldContainer=$('#companies-container');

        fieldContainer.html('');

        data.forEach(company => {
           
            let fieldDiv=$('<div/>',{
                class:'form-inline row py-1'
            });

            let fieldLabel=$('<label/>',{
                class:'col-sm-2 d-none d-sm-inline-block text-left control-label',
                for:'company_price['+company.id+']',
            });

            fieldLabel.html(company.name);

            let fieldInput=$('<input/>',{
                type:'number',
                class:'form-control col-lg-4 col-md-8 col-sm-9 col-10',
                name:'company_price['+company.id+']',
                placeholder:company.name,
                step:'0.01',
                min:'0.00',
                max:'999999.99',
            });

            fieldDiv.append(fieldLabel);
            fieldDiv.append(fieldInput);

            fieldContainer.append(fieldDiv);

        });

    }

    function updateCompanies(){
        let categoryId=$('.modal-selector option:selected').val();

        jQuery.ajax({
            type: 'GET',
            url: '/work_categories/companies/'+categoryId,
            success: function (data) {
                buildFields(data);
            },
            error: function(data) { 
                console.log(data);
            }
        });

    }

    $(document).on('change','.modal-selector',function(){
        updateCompanies();
    });

	$(document).on('submit','#create-form',function (e) {
		e.preventDefault();
        e.stopImmediatePropagation();
		$('#create-form').validate(submitStoreForm());
		return true;
	});

    updateCompanies();

</script>