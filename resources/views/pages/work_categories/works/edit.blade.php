<form
    id='edit-form'>
			
    {{-- CROSS-SITE PROTECTION --}}
    @csrf

    <!-- CATEGORY -->
    @include('inc.forms.dropdown', ['options'=>App\WorkCategory::all(), 'optionLabel'=>'name', 'name'=>'category_id', 'label'=>'Category', 'base'=>$work['work_category_id'], 'class'=>'modal-selector', 'attributes'=>'disabled'])

    <!-- NAME -->
    @include('inc.forms.field',['name'=>'name', 'attributes'=>'required', 'label'=>'Name', 'base'=>$work['name']])

    <!-- UNIT -->
    @include('inc.forms.field',['name'=>'unit', 'attributes'=>'required', 'label'=>'Unit', 'base'=>$work['unit']])

    <div id="companies-container">

        @foreach ($work['companies'] as $company_id=>$company_name)

            @include('inc.forms.field',['type'=>'number', 'name'=>'company_price['.$company_id.']', 'attributes'=>'step=0.01 min=0.01 max=999999.99', 'label'=>$company_name, 'base'=>$work['prices'][$company_id]??''])

        @endforeach

    </div>

    <button id='edit-form-submit' type='submit' class='btn btn-primary'>Save</button>

</form>

<script>


    function submitUpdateForm(){
        let formData=$('#edit-form').serialize();
        $('#edit-form').trigger("reset");
        $.patch(
			'{{ route('work_categories.update_work',$work['id'])}}',
			formData,
			function(json){
				response=JSON.parse(json);
				clearModal();
				sendMessage(response.type,response.message);
				updateTable();
        	}
		).fail(function(xhr, status, error){
			console.log(xhr);
        });
    }

	$(document).on('submit','#edit-form',function (e) {
		e.preventDefault();
        e.stopImmediatePropagation();
        console.log(e);
		$('#edit-form').validate(submitUpdateForm());
		return true;
	});


</script>