		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Opus Innov'</title>

		<link rel="stylesheet" href="{{asset('css/fontawesome.css')}}">
		<link rel="stylesheet" href="{{asset('font-awesome/css/font-awesome.css')}}">
		<link rel="stylesheet" href="{{asset('css/material_icons.css')}}">
		<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
		<link rel="stylesheet" href="{{asset('css/style.css')}}">

		<style>
			body.modal-open {
				overflow: visible;
			}
		</style>