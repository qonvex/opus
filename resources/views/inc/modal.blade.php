		<div id="modal" class="modal fade" role="dialog" style="">
			<div class="modal-dialog modal-dialog" style="transform: translateY(10vh);">

				<div class="modal-content" >
					<div class="modal-header">
						<h4 id="modal-title" class="modal-title">Modal Header</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div id="modal-content" class="modal-body">
					</div>
					<div class="modal-footer">
						<button id="modal-confirm" type="button" class="btn btn-primary" data-dismiss="modal">Confirmer</button>
						<button id="modal-close" type="button" class="btn btn-secondary" data-dismiss="modal">Retour</button>
					</div>
				</div>
			</div>
		</div>
		