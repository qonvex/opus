		<script src='{{asset('js/jquery-3.4.1.min.js')}}'></script>
		<script src='{{asset('js/jquery-validate.min.js')}}'></script>

		<script src='{{asset('js/Chart.min.js')}}'></script>
		<script src='/js/main.js?v={{app('App\Http\Controllers\PagesController')->version()}}'></script>

		<script>
			
			// IMPROMPTU LOGIN

			function impromptuLogin(e){

				let wind=popupWindow('/login','Connexion',window,360,480);

				let wait_for_login=window.setInterval(()=>{
					
					let current_location=wind.location.href;

					console.log(
						current_location.split('/'),
						current_location.split('/').length,
						current_location.split('/').pop(),
						wind.document,
						wind.document.getElementsByName('csrf-token')[0],
						wind.document.getElementsByName('csrf-token')[0].content
					);

					if (current_location.split('/').length==4 &&
						current_location.split('/').pop()=='' &&
						wind.document &&
						wind.document.getElementsByName('csrf-token')[0] &&
						wind.document.getElementsByName('csrf-token')[0].content
					){

						window.document.getElementsByName('csrf-token')[0].content=wind.document.getElementsByName('csrf-token')[0].content;
						wind.close();
						window.setTimeout(()=>{
							window.clearInterval(wait_for_login);
							closeModal();
						},1000);
					}

				},2000);

			}

			function popupWindow(url, title, win, w, h) {
			const y = win.top.outerHeight / 2 + win.top.screenY - ( h / 2);
			const x = win.top.outerWidth / 2 + win.top.screenX - ( w / 2);
			return win.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+y+', left='+x);
	}

			//	MODAL SCRIPT

			function launchModal(title='',content='',close=null,confirm=null){
				
				$('#modal-title').html(title);
				$('#modal-content').html(content);
				
				if(typeof confirm=='function'){
					$('#modal-confirm').off('click.launchModal').on('click.launchModal',confirm).show();
					$('#modal-close').html('Retour');
				}else{
					$('#modal-confirm').hide();
					$('#modal-close').html('Close');
				}

				$('#modal').off('hide.bs.modal.launchModal');
				if(typeof close=='function') $('#modal').on('hide.bs.modal.launchModal',close);

				$('#modal').modal('show');
			}

			function closeModal(){
				$("#modal").modal('hide');    
			} 

			$(document).keyup(function(e){

				if(e.keyCode === 27)
					closeModal();
				}
			
			);

			function sendMessage(type,message){

				alert(Object.values(message.responseJSON.errors).join());

			}
			
			/*
			 *
			 * SNAKE CASE CONVERTER
			 * 
			 * Converts snake case to Capital Words
			 * 
			 */
			String.prototype.snakeToCapitalized=function(){
				return this.replace(/_/g, ' ').replace(/\w\S*/g, function(word){
					return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
				});
			}

			/*
			 *
			 * NUMBER PLACES
			 * 
			 * Converts number in string form to number with commas for places
			 * 
			 */
			String.prototype.places=function(){
				return this.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			}


			/*
			 *
			 * BROWSER HISTORY HANDLER
			 *
			 * Handles pushing pages into the client's browsing history.
			 *
			 */
			function pushHistory(url,title=''){
				let obj = { url: url };
				history.pushState(obj, title, url);
				$('title').html(title!=''?title+' - Opus Innov\'':'Opus Innov\'');
			}

			/*
			 *
			 * AJAX LINK EVENT HANDLER
			 * 
			 * Handles link-click events and fires if no other click-modifiers are present
			 * 
			 */
			function handleClick(el, event, callback){
				if (!event.shiftKey && !event.ctrlKey){
					event.stopImmediatePropagation();
					event.preventDefault();
					callback(el,event);
				}
			}

		</script>


<script>

var mimeTypes={
	aac:'audio/aac',
	abw:'application/x-abiword',
	arc:'application/x-freearc',
	avi:'video/x-msvideo',
	azw:'application/vnd.amazon.ebook',
	bin:'application/octet-stream',
	bmp:'image/bmp',
	bz:'application/x-bzip',
	bz2:'application/x-bzip2',
	csh:'application/x-csh',
	css:'text/css',
	csv:'text/csv',
	// doc:'application/msword',
	// docx:'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
	eot:'application/vnd.ms-fontobject',
	epub:'application/epub+zip',
	gif:'image/gif',
	htm:'text/html',
	html:'text/html',
	ico:'image/vnd.microsoft.icon',
	ics:'text/calendar',
	jar:'application/java-archive',
	jpeg:'image/jpeg',
	jpg:'image/jpeg',
	js:'text/javascript',
	json:'application/json',
	jsonld:'application/ld+json',
	mid:'audio/midi audio/x-midi',
	midi:'audio/midi audio/x-midi',
	mjs:'text/javascript',
	mp3:'audio/mpeg',
	mpeg:'video/mpeg',
	mpkg:'application/vnd.apple.installer+xml',
	odp:'application/vnd.oasis.opendocument.presentation',
	ods:'application/vnd.oasis.opendocument.spreadsheet',
	odt:'application/vnd.oasis.opendocument.text',
	oga:'audio/ogg',
	ogv:'video/ogg',
	ogx:'application/ogg',
	otf:'font/otf',
	png:'image/png',
	pdf:'application/pdf',
	// ppt:'application/vnd.ms-powerpoint',
	// pptx:'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	rar:'application/x-rar-compressed',
	rtf:'application/rtf',
	sh:'application/x-sh',
	svg:'image/svg+xml',
	swf:'application/x-shockwave-flash',
	tar:'application/x-tar',
	tif:'image/tiff',
	tiff:'image/tiff',
	ts:'video/mp2t',
	ttf:'font/ttf',
	txt:'text/plain',
	vsd:'application/vnd.visio',
	wav:'audio/wav',
	weba:'audio/webm',
	webm:'video/webm',
	webp:'image/webp',
	woff:'font/woff',
	woff2:'font/woff2',
	xhtml:'application/xhtml+xml',
	// xls:'application/vnd.ms-excel',
	// xlsx:'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
	xml:'text/xml',
	xul:'application/vnd.mozilla.xul+xml',
	zip:'application/zip',
	['3gp']:'video/3gpp',
	['3g2']:'video/3gpp2',
	['7z']:'application/x-7z-compressed'
}

</script>