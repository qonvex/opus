		<div class="form-inline row py-1 {{$class??''}}">
			<label class="col-sm-2 d-none d-sm-inline-block pl-5 text-left control-label" for="{{$name}}">{{$label}}:</label>
			<input
				type="{{$type ?? 'text'}}"
				class="form-control col-lg-4 col-md-8 col-sm-9 col-10 ml-5 ml-sm-0 mr-1"
				name="{{$name}}"
				placeholder="{{$label}}"
				{{$attributes??''}}
				value="{{old($name, $default??null)?old($name, $default??null):$base??null}}"/>
		</div>