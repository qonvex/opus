
	@php


		if (isset($base)){
			$baseIds=[];
			foreach ($base as $baseItem){
				$baseIds[]=$baseItem['id'];
			}

			
		//base Id-ify

			$baseIdIfied=[];
			foreach ($base as $baseItem){
				$baseIdIfied[$baseItem['id']]=$baseItem['pivot'][$baseDatefield];
			}


		}


		
	@endphp

<div class="form-group col my-5">
	@foreach($options as $option)
	<?php $option=$option->toArray() ?>
		<div class="form-inline row py-1">
			<label class="label-control col-4 col-md-2" for="{{$name}}[{{$option['id']}}]">{{$option['name']}}</label>
			<input
				type="checkbox"
				class="form-control col-1 mx-1"
				name="{{$name}}[{{$option['id']}}]"
				value="{{$option['id']}}"
				@if (old('submit',null)!=null)
					@if (in_array($option['id'],old($name)??[]))
						checked
					@endif
				@else
					@if (in_array($option['id'],($baseIds??[])))
						checked
					@endif
				@endif
				>
			@if (isset($datefield))
			<input
				type="date"
				class="form-control col-6 col-md-3"
				max="{{ today()->toDateString() }}"
				name="{{ $datefield.'['.$option['id'].']' }}"
				value="{{ old($datefield)[$option['id']] ?? $baseIdIfied[$option['id']] ?? today()->toDateString() }}">

				<script>
					


				</script>

			@endif
			@php
				//print_r($base[$option['id']]);
			@endphp
		</div>
	@endforeach
</div>