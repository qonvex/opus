<div class="form-inline row py-1 pr-1 {{$class??''}}">
	<label class="col-2 d-sm-inline-block pl-5 text-left control-label" for="{{$name}}">{{$label}}:</label>
	<select
	name="{{$name}}"
	class="form-control col-9 col-md-8 col-lg-4 {{$class??''}}"
	{{$attributes??''}}>

	<option disabled selected value> -- Select {{$label}} -- </option>

		@foreach($options->pluck($optionLabel,'id') as $id=>$label)
				<option 
					value="{{$id}}"
					@if($id==old($name)||$id==($base??null))
							selected
					@endif
				>
					{{$label}}
				</option>
		@endforeach
	</select>

	@if($removeButton??false)
		<button type="button" class="btn btn-primary remove-button">X</button>
	@endif
</div>