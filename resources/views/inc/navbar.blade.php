{{-- DEFINE ROUTES --}}

@php
    $dashboardURL=route('home');
    $prospectsURL=route('prospects.index');
    $clientsURL='#';//route('clients.index');
    $companiesURL=route('companies.index');
    $projectsURL='#';//route('prospects.index');
@endphp

    <!-- HEADER -->
    <div class="header row">

        <!-- LOGO -->
        <div class="left-header">
            <div class="logo">
                <a href="/" class=''>
                    <img class="img-responsive" src="{{asset('img/page-logo.jpg')}}">
                </a>
            </div>
        </div>	

        <div class="right-header ">
            <div class="search-and-admin row">
                <div class="search col-lg-10 col-md-12 col-xs-8">
                    <div class="row">
                        <button class="btn btn-sm">
                            <div class="search-icon">
                                <span class="fa fa-search"></span>
                            </div>
                        </button>
                        <input class="input-search input-sm col-md-6 col-sm-9 col-xs-5" type="text" placeholder="Search" value="">
                    </div>
                </div>

                <div class="admin text-right col-lg-2 col-md-3 ">
                    <div class="dropdown">
                        <button class="btn btn-sm" id="dropdownMenuLink" data-toggle="dropdown"  aria-expanded="false">
                            <span class="fa fa-user admin-icon" style="font-size: 30px;"></span> &nbsp &nbsp
                            <span><p>Admin</p></span> &nbsp&nbsp
                            <span class="fa fa-angle-down down-arrow-icon"></span>
                        </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Admin account</a>
                            <a class="dropdown-item" href="#">Setting</a>
                            <a class="dropdown-item" href="#">Help</a>
                        </div>
                    </div>
                </div>

                <div class="navigation col-xs-4">
                    <nav class="navbar navbar-default navbar-dark navbar-fixed-top">
                        <div class="navbar-header">
                            <button class="navbar-toggle btn" type="button"data-toggle="collapse" data-target="#myNavbar">
                                <img src="{{asset('img/menu.png')}}">
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="#" id="dropdownMenuLink" data-toggle="dropdown"  aria-expanded="false">Admin</a>
                                    <div class="dropdown">
                                        
                                            <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Admin account</a>
                                            <a class="dropdown-item" href="#">Setting</a>
                                            <a class="dropdown-item" href="#">Help</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="{{$dashboardURL}}">Accueil</a>
                                </li>
                                <li>
                                    <a href="{{$prospectsURL}}">Prospects</a>
                                </li>
                                <li>
                                    <a href="{{$clientsURL}}">Clients</a>
                                </li>
                                <li>
                                    <a href="{{$companiesURL}}">Enterprise</a>
                                </li>
                                <li>
                                    <a href="?page=projets">Projets</a>
                                </li>
                                <li>
                                    <a href="?page=Factures">Factures</a>
                                </li>
                                <li>
                                    <a href="#">Configuration</a>
                                </li>
                            </ul>
                        </div>
                    </nav>	
                </div>

            </div>
        </div>		
    </div>

    <div class="content row">


    <!-- START SIDEBAR -->
    <div class=" sidebar" id="mySidebar">
                    
        <!-- SIDEBAR MENU -->
        <div class="menu">
            <ul class="list-unstyled">
                <li>
                    <a class="nav" href="{{$dashboardURL}}">
                        <span class="material-icons" >dashboard</span>
                        <span>Accueil</span>
                    </a>
                </li>
                <li>
                    <a class="nav" href="{{$prospectsURL}}">
                    <span>
                        <img class="icon1 img-responsive" src="{{asset('img/prospects.png')}}">
                        <img class="icon2 img-responsive" src="{{asset('img/prospects2.png')}}">
                    </span>
                    <span>Prospects</span></a></li>
                <li>
                    <a class="nav" href="{{$clientsURL}}">
                    <span class="fas fa-users"></span>
                    <span>Clients</span></a>
                </li>
                <li>
                    <a class="nav" href="{{$companiesURL}}">
                    <span class="material-icons">business_center</span>	
                    <span>Enterprise</span></a>
                </li>
                <li>
                    <a class="nav" href="{{$projectsURL}}">
                    <span class="fas fa-layer-group"></span><span>Projects</span></a>
                </li>
                <li>
                    <a class="nav" href="?page=factures">
                    <span class="material-icons">find_in_page</span>
                    <span>Factures</span></a>
                </li>
                <li>
                    <a class="nav" href="?page=configuration">
                    <span class="material-icons">settings</span>
                    <span>Configuration</span></a>
                </li>
            </ul>
        </div>

        <!-- SIDEBAR FOOTER -->
        <div class="footer">
            <p class="text-center">Copyright 2019 &copy</p>
        </div>
        
    </div>
    <!-- END SIDEBAR -->
