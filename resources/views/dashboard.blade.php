@extends('layouts.app')

@section('content')


<!-- START CONTENT -->
<div class="page-content">
    <!-- START ACCUEIL -->
    <div class="accueil">
        <!-- START GRAPH -->
        <div class="line-graph row">
            <!-- GRAPH 1 -->
            <div class="col-md-6">
                <div class="graph">
                    <div class="title">
                        <h6>SUIVI DU CHIFFRE D'AFFAIRES PAR MOIS</h6>
                    </div>

                    <div class="graph-content container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="upper-text">
                                    <p>Chiffre d'affaires réalisé :<strong> 46k €</strong></p>	
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="upper-text">
                                    <p>Chiffre d'affaires réalisé :<strong> 46k €</strong></p>	
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="lower-text">
                                    <p>Objectif chiffre d'affaires mensuel: &nbsp &nbsp &nbsp <strong>XXX XXX €</strong></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="lower-text">
                                    <p>Objectif chiffre d'affaires mensuel: &nbsp &nbsp &nbsp <strong>XXX XXX €</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END GRAPH 1 -->


            <!-- START GRAPH 2 -->
            <div class="col-md-6">
                <div class="graph">
                    <div class="title">
                        <h6>SUIVI DU CHIFFRE D'AFFAIRES PAR MOIS</h6>
                    </div>

                    <div class="graph-content container">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="upper-text">
                                    <p>Chiffre d'affaires réalisé :<strong> 46k €</strong></p>	
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="upper-text">
                                    <p>Chiffre d'affaires réalisé :<strong> 46k €</strong></p>	
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <canvas id="lineChart2"></canvas>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="lower-text">
                                    <p>Objectif chiffre d'affaires mensuel: &nbsp &nbsp &nbsp <strong>XXX XXX €</strong></p>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="lower-text">
                                    <p>Objectif chiffre d'affaires mensuel: &nbsp &nbsp &nbsp <strong>XXX XXX €</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END GRAPH -->
            
        <!-- START TABLEAU PROSPECTS -->
        <div class="table-prospect row">
            <div class="col-md-12">
                <div class="title row">
                    <h6>TABLEAU PROSPECTS</h6>
                </div>
                <div class="table-wrap table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>RAF</th>
                                <th>R1 à R5</th>
                                <th>Compte rendu signé</th>
                                <th>Estimatif tarifaire</th>
                                <th>Vente</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END TABLEAU PROSPECTS -->

        <!-- START TABLEAU CLIENTS -->
        <div class="table-client row">
            <div class="col-md-12">
                <div class="title row">
                    <h6>TABLEAU CLIENTS</h6>
                </div>
                <div class="table-wrap table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Prospect</th>
                                <th>RAF</th>
                                <th>Compte rendu</th>
                                <th>Estimatif tarifaire</th>
                                <th>Vente</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                                <td>John Doe</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- END TABLEAU CLIENTS -->

        <!-- START TABLEAU AVANCÉ DES PROJETS -->
        <div class="table-project row">
            <div class="col-md-12">
                <div class="title row">
                    <h6>TABLEAU AVANCÉ DES PROJETS</h6>
                </div>
                <div class="table-wrap table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Nom projet</th>
                                <th>Email</th>
                                <th>Adresse</th>
                                <th>Téléphone</th>
                                <th>Avancé du projet</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>John Doe</td>
                                <td>xxx@email.com</td>
                                <td>xxxx Quimper</td>
                                <td>06xxxxxxxx</td>
                                <td>
                                        <div class="circle-check">
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot checked"></span>
                                            <span class="dot"></span>
                                        </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
        </div>
        <!-- END TABLEAU AVANCÉ DES PROJETS -->

        <!-- START TABLEAU SUIVI ACTIVITÉS MOIS -->
        <div class="table-activities row">
            <div class="col-md-12">
                <div class="title row">
                    <h6>TABLEAU SUIVI ACTIVITÉS MOIS</h6>
                </div>
                <div class="table-wrap table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>Nom projet</th>
                                <th>Email</th>
                                <th>Adresse</th>
                                <th>Téléphone</th>
                                <th>Avancé du projet</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>10</td>
                                <td>4</td>
                                <td>2</td>
                                <td>2</td>
                                <td>1</td>
                            </tr>
                        </tbody>
                    </table>
                </div>	
            </div>
        </div>
        <!-- END TABLEAU SUIVI ACTIVITÉS MOIS -->
    </div>
    <!-- END ACCUEIL -->
</div>
<!-- END CONTENT -->


@endsection

@section('scripts')
    <script src='{{asset('js/runChart.js')}}'></script>
@endsection