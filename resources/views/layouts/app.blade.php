<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>

		@include('inc.header')

	</head>

	<body>

		

		@include('inc.modal')

		<div id="app">
		@auth
		
		<App ref="app"/>
		
		@endauth
		@yield('content')
		</div>

		
		@auth

		<script id="initialization">
			globalStartJson=`<?php echo(json_encode($data)??null); ?>`;

			try{
				
				globalStartData=JSON.parse(
					globalStartJson.replace(/\n/g, "\\n")
					.replace(/\r/g, "\\r")
					.replace(/\t/g, "\\t")
					.replace(/\f/g, "\\f"));

			}catch(e){

				window.location.reload();

			}
			serverAppVersion='<?php echo $version; ?>';
			gzsize=<?php print($gzsize); ?>;

		</script>		

		@endauth
	
		@auth

		<div id="loadingbox" style="position:absolute; height:100vh; width:100vw; background-color:#dde; top:0; 
			opacity: 0;
			transition: opacity 0.5s ease-in-out;
			">
			<img id="appLoading" src="/img/bigloading.svg" style="position: relative; top:25vh; height:20vh;" alt="Loading...">
			<div class="progress" style="position:relative; top:50vh">
				<div id="thebar" class="progress-bar" style="
				width: 0%;
				transition-property: width;
				transition-timing-function: ease-out;
				transition-duration: 0.5s;
				background-color:#29d;
				"></div>
			</div>

			<script id="loadscript">

			function beginLoadingApp(){

				let localAppVersion=localStorage.getItem('opusAppVersion');
				let localApp=localStorage.getItem('opusApp');

				if (localAppVersion&&(localAppVersion==serverAppVersion)&&localApp){
					$.globalEval(localApp);
					document.getElementById('loadingbox').remove();
					document.removeEventListener('DOMContentLoaded',beginLoadingApp,false);
					window.beginLoadingApp=undefined;
					return;
				}
			
				let req = new XMLHttpRequest();
				let box = document.getElementById('loadingbox');
				let bar = document.getElementById('thebar');

				box.style.opacity=1;


				// report progress events
				req.addEventListener("progress", function(event) {
						let percentComplete = event.loaded / gzsize;
						bar.style.width=percentComplete*101+'%';
				}, false);

				req.addEventListener("load", function(event) {
					let e = event.target;
					box.style.opacity=0;

					localStorage.setItem('opusAppVersion',serverAppVersion);
					localStorage.setItem('opusApp',e.responseText);
					
					$.globalEval(e.responseText);
					
					setTimeout(function(){
						document.getElementById('loadingbox').remove()
						document.removeEventListener('DOMContentLoaded',beginLoadingApp,false);
						window.beginLoadingApp=undefined;
					},1000)
				}, false);

				req.open("POST", "/js/app.js");
				req.send();

			}

			document.getElementById('initialization').remove();

			document.addEventListener('DOMContentLoaded',beginLoadingApp,false);
			
			</script>
		</div>
		@endauth

	@include('inc.scripts')
	
	</body>
</html>
