/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//	TEST COMPONENT
// Vue.component('Test', require('./components/Test.vue').default);

//	COMMON COMPONENTS
Vue.component('App', require('./components/App.vue').default);
Vue.component('Navbar', require('./components/modules/Navbar.vue').default);
Vue.component('Header', require('./components/modules/Header.vue').default);
Vue.component('Loading', require('./components/modules/Loading.vue').default);
Vue.component('FileWindow', require('./components/modules/FileWindow.vue').default);
Vue.component('StatusWindow', require('./components/modules/StatusWindow.vue').default);
Vue.component('DatePick', require('./components/modules/DatePick.vue').default);
Vue.component('FilePicker', require('./components/modules/FilePicker.vue').default);

//	PAGES COMPONENTS
Vue.component('Dashboard', require('./components/pages/Dashboard.vue').default);

Vue.component('Prospects', require('./components/pages/Prospects.vue').default);
	//	PROSPECTS PAGE
	Vue.component('ProspectsIndex', require('./components/pages/prospects/ProspectsIndex.vue').default);
		Vue.component('ProspectRow', require('./components/pages/prospects/prospectsIndex/ProspectRow.vue').default);
	Vue.component('ProspectsCreate', require('./components/pages/prospects/ProspectsCreate.vue').default);
    Vue.component('ProspectsShow', require('./components/pages/prospects/ProspectsShow.vue').default);

Vue.component('Clients', require('./components/pages/Clients.vue').default);
	//	PROSPECTS PAGE
	Vue.component('ClientsIndex', require('./components/pages/clients/ClientsIndex.vue').default);
		Vue.component('ClientRow', require('./components/pages/clients/clientsIndex/ClientRow.vue').default);
	Vue.component('ClientsCreate', require('./components/pages/clients/ClientsCreate.vue').default);
    Vue.component('ClientsShow', require('./components/pages/clients/ClientsShow.vue').default);

Vue.component('Companies', require('./components/pages/Companies.vue').default);
    //  COMPANIES PAGE
	Vue.component('CompaniesIndex', require('./components/pages/companies/CompaniesIndex.vue').default);
	Vue.component('CompaniesCreate', require('./components/pages/companies/CompaniesCreate.vue').default);
    Vue.component('CompaniesShow', require('./components/pages/companies/CompaniesShow.vue').default);

Vue.component('WorkCategories', require('./components/pages/WorkCategories.vue').default);
    //  WORK_CATEGORIES PAGE
    Vue.component('WorkCategoriesIndex', require('./components/pages/workCategories/WorkCategoriesIndex.vue').default);
    Vue.component('WorkCategoriesCreate', require('./components/pages/workCategories/WorkCategoriesCreate.vue').default);
    Vue.component('WorkCategoriesShow', require('./components/pages/workCategories/WorkCategoriesShow.vue').default);

Vue.component('Projects', require('./components/pages/Projects.vue').default);
    //  COMPANIES PAGE
	Vue.component('ProjectsIndex', require('./components/pages/projects/ProjectsIndex.vue').default);
	Vue.component('ProjectsCreate', require('./components/pages/projects/ProjectsCreate.vue').default);
    Vue.component('ProjectsShow', require('./components/pages/projects/ProjectsShow.vue').default);

Vue.component('Bills', require('./components/pages/Bills.vue').default);
    //  COMPANIES PAGE
	Vue.component('BillsIndex', require('./components/pages/bills/BillsIndex.vue').default);
    Vue.component('BillsShow', require('./components/pages/bills/BillsShow.vue').default);

Vue.component('Config', require('./components/pages/Config.vue').default);
    //  COMPANIES PAGE
	Vue.component('ConfigIndex', require('./components/pages/config/ConfigIndex.vue').default);
	Vue.component('ConfigCreate', require('./components/pages/config/ConfigCreate.vue').default);
    Vue.component('ConfigShow', require('./components/pages/config/ConfigShow.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	el: '#app',
	created(){

		window.onpopstate = function(event) {
			let resource=document.location.pathname.substring(1).split('/');
			let args=document.location.search.substring(1);
			args=args?args.split('&').reduce(function(args,arg){
				let pair=arg.split('=');
				args[pair[0]]=pair[1];
				return args;
			},[]):null;
			if (resource.length==1){
				//INDEX
				if (args && args['page']){
					//PAGE
					app.$refs.app.navigateIndex(resource[0],args['page']);
				}else{
					app.$refs.app.navigateIndex(resource[0],1);
				}
			}else if (resource.includes('create')){
				//CREATE
				app.$refs.app.navigateCreate(resource[0]);
			}else{
				//SHOW
				app.$refs.app.navigateShow(resource[0],resource[resource.length-1]);
			}
		};
	},
	mounted(){
		document.body.prepend(this.$el.firstChild);
	},
	methods:{
		failHandler(response){
			console.log(response);
			this.$refs.app.$refs.loading.fail();
			try{
				switch(response.status){
					case 0:
						launchModal("Erreur réseau","La connexion a expiré, veuillez vérifier votre réseau.");
						break;
					case 401:
						launchModal("Non Authentifié","Veuillez vous <button id='reconnect-button' type='button' onclick='impromptuLogin()'>connecter</a>");
						break;
					case 403:
						launchModal("Restrictif","Vous n'êtes pas autorisé à accéder à cette ressource.");
						break;
					case 404:
						launchModal("Ressource Introuvable","La ressource demandée n'a pas été trouvée.");
						break;
					case 422:
						response=response.responseJSON;
						launchModal("Données invalides",response.errors?Object.values(response.errors).reduce(function(text,message){
							return text+message+'<br>';
						},''):response.message);
						break;
					case 500:
						launchModal("Erreur de serveur","Il y a eu une erreur sur le serveur. Veuillez contacter l'assistance <details><summary>avec les informations suivantes :</summary><pre>"+response.responseText+"</pre></details>");
						break;
					default:
						console.log(response);
						response=response.responseJSON;
						launchModal(response.message,response.errors?Object.values(response.errors).reduce(function(text,message){
							return text+message+'<br>';
						},''):response.message);
						break;
					}

			}catch(e){

				launchModal("Erreur inconnue","Une erreur inconnue est survenue. S'il vous plaît contacter le support. ")
			}
		},
		startLoading(){
			this.$refs.app.$refs.loading.start(); 
		},
		doneLoading(){
			this.$refs.app.$refs.loading.done();
		},
		failLoading(){
            this.$refs.app.$refs.loading.fail();
        },
		warnLoading(){
            this.$refs.app.$refs.loading.warn();
        }
	}
});

